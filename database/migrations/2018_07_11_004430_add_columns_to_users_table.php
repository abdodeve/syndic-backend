<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('main')->table('users', function (Blueprint $table) {
            $table->integer('fk_roles')->after('id')->nullable();
            $table->boolean('active')->after('remember_token')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('main')->table('users', function (Blueprint $table) {
            $table->dropColumn('fk_roles');
        });
    }
}
