<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tenant')->create('budget', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_copropriete');
            $table->string('libelle')->nullable();
            $table->double('montant_actuel')->nullable();
            $table->double('montant_anterieure')->nullable();
            $table->double('montant_reste_anterieure')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tenant')->dropIfExists('budget');
    }
}
