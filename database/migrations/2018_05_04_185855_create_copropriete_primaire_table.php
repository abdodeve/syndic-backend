<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoproprietePrimaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tenant')->create('copropriete_primaire', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_type_copropriete');
            $table->string('nom');
            $table->string('slug')->nullable();
            $table->string('ville')->nullable();
            $table->string('adresse')->nullable();
            $table->longText('description')->nullable();
            $table->string('code_postale')->nullable();
            $table->string('pays')->nullable();
            $table->date('date_reprise')->nullable();
            $table->string('plan')->nullable();
            $table->string('designation_plan')->nullable();
            $table->string('titre_foncier')->nullable();
            $table->double('surface_totale')->nullable();
            $table->double('totale_tantieme')->nullable();
            $table->double('penalite_taux')->nullable();
            $table->boolean('is_penalite_exist')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tenant')->dropIfExists('copropriete_primaire');
    }
}
