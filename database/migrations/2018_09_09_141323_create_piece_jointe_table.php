<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePieceJointeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tenant')->create('piece_jointe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_facture')->nullable();
            $table->integer('fk_paiement')->nullable();
            $table->string('fk_copropriete')->nullable();
            $table->string('name')->nullable();
            $table->string('source')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tenant')->dropIfExists('piece_jointe');
    }
}
