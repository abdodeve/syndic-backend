<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class EventCleanTrash extends Migration
{
    /**
     * Run the migrations.
     *
     * ********** VERYYYYYYYYYYYYY IMPORTANT ************
     *
     * To Start Event Execute :  SET GLOBAL event_scheduler = ON ;
     *
     * @return void
     */
    public function up()
    {
//      DB::unprepared(' SET GLOBAL event_scheduler="ON" ;
//        CREATE EVENT e_CleanTrash
//        ON SCHEDULE
//          EVERY 1 HOUR
//        COMMENT \'Clears out all rows have deleted as status\'
//        DO
//          DELETE FROM '.DB::getTablePrefix().'proprietaire WHERE status LIKE \'deleted\'
//          AND time_to_sec(timediff( NOW(), updated_at)) / 3600 > 24;') ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('SET GLOBAL event_scheduler="OFF" ;
        DROP EVENT IF EXISTS e_CleanTrash;') ;
    }
}
