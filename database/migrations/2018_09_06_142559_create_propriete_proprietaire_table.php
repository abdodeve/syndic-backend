<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProprieteProprietaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tenant')->create('propriete_proprietaire', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_copropriete');
            $table->integer('fk_propriete')->nullable();
            $table->integer('fk_proprietaire')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tenant')->dropIfExists('propriete_proprietaire');
    }
}
