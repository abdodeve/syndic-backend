<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tenant')->create('facture', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_copropriete');
            $table->integer('fk_propriete');
            $table->string('num_facture')->nullable();
            $table->date('date_facture')->nullable();
            $table->string('prestation')->nullable();
            $table->float('n_piece')->nullable();
            $table->string('libelle')->nullable();
            $table->float('montant_facture')->nullable();
            $table->float('montant_penalite')->nullable();
            $table->string('piece_jointe')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tenant')->dropIfExists('facture');
    }
}
