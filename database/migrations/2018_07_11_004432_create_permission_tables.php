<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::connection('tenant')->create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_groupe_permissions')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::connection('tenant')->create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::connection('tenant')->create('role_has_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_id');
            $table->integer('role_id');
            $table->timestamps();
        });

        Schema::connection('tenant')->create('groupe_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tenant')->dropIfExists('role_has_permissions');
        Schema::connection('tenant')->dropIfExists('roles');
        Schema::connection('tenant')->dropIfExists('permissions');
        Schema::connection('tenant')->dropIfExists('groupe_permissions');
    }
}
