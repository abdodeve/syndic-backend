<?php

use Illuminate\Database\Seeder;

class PermissionsTablesSeeder extends Seeder
{
    /**
     * Insert Permissions seeds
     *
     * @return void
     */
    public function run()
    {

        // Use Tenant Connection
        Config::set('database.default', 'tenant');

        // Delete
        DB::table('roles')->delete();
        DB::table('role_has_permissions')->delete();
        DB::table('roles')->delete();
        DB::table('groupe_permissions')->delete();
        DB::table('permissions')->delete();

        // Roles - Insert default
        $roles =  [
                        [
                            'id'   => 1,
                            'name' => "super-admin"
                        ],
                        [
                            'id'   => 2,
                            'name' => "admin"
                        ],
                        [
                            'id'   => 3,
                            'name' => "utilisateur"
                        ],

                  ];
        DB::table('roles')->insert($roles);

        // role_has_permissions - Insert default
        $permissions =  [
            [
                'role_id'       => '1',
                'permission_id' => '1'
            ],
            [
                'role_id'       => '2',
                'permission_id' => '2'
            ],
        ];
        DB::table('role_has_permissions')->insert($permissions);

        // Insert default groupe_permissions
        $groupes =  [
            [
                'id'          => 1,
                'name'        => "generale",
                'description' => "Générale"
            ],
            [
                'id'          => 2,
                'name'        => "module_comptabilite",
                'description' => "Module comptabilite"
            ],
            [
                'id'          => 3,
                'name'        => "super-admin-groupe",
                'description' => 'super-admin-groupe'
            ],
            [
                'id'          => 4,
                'name'        => "proprietaire",
                'description' => 'Proprietaire'
            ],
            [
                'id'          => 5,
                'name'        => "propriete",
                'description' => 'Propriete'
            ],
            [
                'id'          => 6,
                'name'        => "propriete_proprietaire",
                'description' => 'ProprieteProprietaire'
            ],
            [
                'id'          => 7,
                'name'        => "copropriete_primaire",
                'description' => 'CoproprietePrimaire'
            ],
        ];
        DB::table('groupe_permissions')->insert($groupes);


        // permissions - Insert default
        $permissions =  [
            /*|--------------|
              | generale     |
              |--------------|*/
            [
                'name'                      => "super-admin-permission",
                'description'               => "Super admin",
                'fk_groupe_permissions'  => null,
            ],
            [
                'name'                      => "admin-permission",
                'description'               => "Administrer",
                'fk_groupe_permissions'  => 1,
            ],
            [
                'name'                      => "restore",
                'description'               => "Restauration",
                'fk_groupe_permissions'  => 1,
            ],
            /*|--------------|
              | proprietaire |
              |--------------|*/
            [
                'name'                      => "display_proprietaire",
                'description'               => "Afficher proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "create_proprietaire",
                'description'               => "Créer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "update_proprietaire",
                'description'               => "Modifier proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "delete_proprietaire",
                'description'               => "Supprimer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "display_proprietaire",
                'description'               => "Créer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "update_proprietaire",
                'description'               => "Modifier proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "delete_proprietaire",
                'description'               => "Supprimer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            /*|--------------|
              | propriete    |
              |--------------|*/
            [
                'name'                      => "display_propriete",
                'description'               => "Afficher propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "create_propriete",
                'description'               => "Créer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "update_propriete",
                'description'               => "Modifier propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "delete_propriete",
                'description'               => "Supprimer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "display_propriete",
                'description'               => "Créer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "update_propriete",
                'description'               => "Modifier propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "delete_propriete",
                'description'               => "Supprimer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            /*|---------------------------|
              | propriete_proprietaire    |
              |---------------------------|*/
            [
                'name'                      => "display_propriete_proprietaire",
                'description'               => "Afficher ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "create_propriete_proprietaire",
                'description'               => "Créer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "update_propriete_proprietaire",
                'description'               => "Modifier ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "delete_propriete_proprietaire",
                'description'               => "Supprimer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "display_propriete_proprietaire",
                'description'               => "Créer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "update_propriete_proprietaire",
                'description'               => "Modifier ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "delete_propriete_proprietaire",
                'description'               => "Supprimer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],

            /*|---------------------------|
              | copropriete_primaire      |
              |---------------------------|*/
            [
                'name'                      => "display_copropriete_primaire",
                'description'               => "Afficher CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "create_copropriete_primaire",
                'description'               => "Créer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "update_copropriete_primaire",
                'description'               => "Modifier CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "delete_copropriete_primaire",
                'description'               => "Supprimer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "display_copropriete_primaire",
                'description'               => "Créer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "update_copropriete_primaire",
                'description'               => "Modifier CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "delete_copropriete_primaire",
                'description'               => "Supprimer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],

            /*|---------------------------|
              | type_copropriete          |
              |---------------------------|*/
            [
                'name'                      => "display_type_copropriete",
                'description'               => "Afficher TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],
            [
                'name'                      => "create_type_copropriete",
                'description'               => "Créer TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],
            [
                'name'                      => "update_type_copropriete",
                'description'               => "Modifier TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],
            [
                'name'                      => "delete_type_copropriete",
                'description'               => "Supprimer TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],
            [
                'name'                      => "display_type_copropriete",
                'description'               => "Créer TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],
            [
                'name'                      => "update_type_copropriete",
                'description'               => "Modifier TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],
            [
                'name'                      => "delete_type_copropriete",
                'description'               => "Supprimer TypeCopropriete",
                'fk_groupe_permissions'  => 8,
            ],

           /*|---------------------------|
             | facture                   |
             |---------------------------|*/
            [
                'name'                      => "display_facture",
                'description'               => "Afficher Facture",
                'fk_groupe_permissions'  => 9,
            ],
            [
                'name'                      => "create_facture",
                'description'               => "Créer Facture",
                'fk_groupe_permissions'  => 9,
            ],
            [
                'name'                      => "update_facture",
                'description'               => "Modifier Facture",
                'fk_groupe_permissions'  => 9,
            ],
            [
                'name'                      => "delete_facture",
                'description'               => "Supprimer Facture",
                'fk_groupe_permissions'  => 9,
            ],
            [
                'name'                      => "display_facture",
                'description'               => "Créer Facture",
                'fk_groupe_permissions'  => 9,
            ],
            [
                'name'                      => "update_facture",
                'description'               => "Modifier Facture",
                'fk_groupe_permissions'  => 9,
            ],
            [
                'name'                      => "delete_facture",
                'description'               => "Supprimer Facture",
                'fk_groupe_permissions'  => 9,
            ],


        ];
        DB::table('permissions')->insert($permissions);
    }
}
