<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Call Other Seeders
        $this->call([
            PermissionsTablesSeeder::class,
        ]);

        // ClientsApp
        factory(App\Models\ClientsAppModel::class, 1)->create() ;

        // Users
        factory(App\User::class, 1)->create() ;

        // ProprietaireModel
        factory(App\Models\ProprietaireModel::class, 50)->create() ;

        // ProprieteModel
        factory(App\Models\ProprieteModel::class, 50)->create() ;

        // CoproprietePrimaireModel
        factory(App\Models\CoproprietePrimaireModel::class, 1)->create() ;

        // CoproprieteModel
        factory(App\Models\CoproprieteModel::class, 2)->create() ;
    }
}
