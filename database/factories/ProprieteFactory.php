<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ProprieteModel::class, function (Faker $faker) {
    // Use Tenant Connection
    Config::set('database.default', 'tenant');
    DB::table('propriete')->delete();
    return [
        'fk_copropriete' => 1,
        'num_propriete' => $faker->randomNumber(4),
        'type_copropriete' => $faker->sentence(1),
        'batiment' => $faker->sentence(2),
        'status' => 'active'
    ];
});
