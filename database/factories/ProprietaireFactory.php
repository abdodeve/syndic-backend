<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ProprietaireModel::class, function (Faker $faker) {
    // Use Tenant Connection
    Config::set('database.default', 'tenant');
    DB::table('proprietaire')->delete();
    return [
        'fk_copropriete' => 1,
        'nom' => $faker->firstName,
        'prenom' => $faker->lastName,
        'titre' => $faker->sentence(3),
        'status' => 'active'
    ];
});
