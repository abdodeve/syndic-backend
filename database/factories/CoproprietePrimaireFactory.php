<?php

use Faker\Generator as Faker;

$factory->define(App\Models\CoproprietePrimaireModel::class, function (Faker $faker) {
    // Use Tenant Connection
    Config::set('database.default', 'tenant');
    DB::table('copropriete_primaire')->delete();
    return [
        'id'  => 1,
        'nom' => 'Res.' . $faker->firstName . $faker->lastName,
        'slug' => $faker->lastName,
        'ville' => $faker->city,
        'status' => 'active'
    ];
});
