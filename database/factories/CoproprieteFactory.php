<?php

use Faker\Generator as Faker;

$factory->define(App\Models\CoproprieteModel::class, function (Faker $faker) {
    // Use Tenant Connection
    Config::set('database.default', 'tenant');
    DB::table('copropriete')->delete();
    return [
        'fk_syndic' => 1,
        'fk_copropriete_primaire' => 1,
        'exercice' => $faker->dateTimeThisCentury->format('Y'),
        'status' => 'active',
    ];
});
