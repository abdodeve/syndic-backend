<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    // Use Main Connection
    Config::set('database.default', 'main');
    DB::table('users')->delete();
    return [
        'id'    => 1,
        'fk_roles'   => '1',
        'fk_clients_app' => 1,
        'name' => 'super-admin', // $faker->name,
        'email' => 'abdelhadi.deve@gmail.com', // $faker->unique()->safeEmail,
        'password' => bcrypt('123'), // secret
        'remember_token' => str_random(10),
    ];
});