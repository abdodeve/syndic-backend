<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ClientsAppModel::class, function (Faker $faker) {
    // Use Main Connection
    Config::set('database.default', 'main');
    DB::table('clients_app')->delete();
    return [
        'id'            => 1,
        'fk_plan'       => 1,
        'db_name'       => Config::get('database.connections.tenant.database'),
        'db_host'       => Config::get('database.connections.tenant.host'),
        'db_username'   => Config::get('database.connections.tenant.username'),
        'db_password'   => Config::get('database.connections.tenant.password'),
    ];
});
