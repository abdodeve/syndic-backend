# E-Syndic SaaS

RestAPI - Back-End for Condo mangement 

The following services are available via HTTP API (REST API)

## Getting Started

E-Syndic is a web application for online condominium management in SAAS mode (software as a service) integrating the accounting, the extranet and the module of the general assembly. With our software, you can manage your homes wherever you are, just have your PC/Mobile and internet access.

### Requirements

* For Perfect environement, I recommended to use Vagrant/homstead-box

* Else be sure you have these requirements :

* Web server software: Apache, Nginx, etc
* PHP 7.1 or higher with PDO drivers for MySQL, PgSQL enabled
* MySQL 5.6 / MariaDB 10.0 or higher for spatial features in MySQL
* Composer 1.6.5 or Higher


### Prerequisites & Configuration

After clonning project assuming you have at least two DBs:

Create at least two databases (One for main connection & Other for tenant connection)

main connection: have static database

tenant connection: have dynmaic database depend on wich user is loggedIn

Please check ```config/database.php ``` to bind connection with Databases.


* I highly recommande for a quick jump to: 

* Make a fresh database for [Main connection] called: "syndic_main"
* Make another fresh database for [Tenant connection] called: "syndic_tenant1"


### Installing


Then Run these commands:

```
# composer install
```

```
# composer dump-autoload
```

```
# php artisan migrate
```

```
# php artisan db:seed
```

```
# php artisan passport:install
```

## Running the tests

Use postman for the test


### Get Access token

url: ```http://your_hostname/oauth/token```
methode: POST
body: 
```
{
        "grant_type": "password",
        "client_id": "your_client_id",
        "client_secret": "your_secret_key",
        "username": "your_user_name",
        "password": "your_password"
}
```
headers:
```
{
	Accept:application/json,
	Content-Type:application/json,
	Authorization,:"Bearer your_access_token"
}
```

### Call An Api

url: ``` http://your_hostname/api/proprietaire/fetch ```

methode: POST

body: ```{"fk_copropriete": 1}```

headers:
```
{
	Accept:application/json,
	Content-Type:application/json,
	Authorization,:"Bearer your_access_token"
}
```

### Result of this test

This test retrieve array of all proprietaires which have 1 as fk_copropriete

```
    "proprietaires": [
        {
            "id": 103,
            "fk_copropriete": 1,
            "nom": "Tuodry",
            "prenom": "Grady",
            "cin": G562347,
            "profession": Account manager,
            "titre": "Soluta sit vel quia.",
            "ville": Roma,
            "adresse_1": 142 New island,
            "adresse_2": 23 Jerssy parc,
            "email_1": tody@gmail.com,
            "email_2": tody_grad@gmail.com,
            "tel_1": 0674423569,
            "tel_2": 0536741236,
            "code_postale": 413200,
            "description": Old client,
            "status": "active",
        },
        .
        .
        .
        }]
```

## Deployment

This backend is running on lamp stack please be sure to have the above requirements

## Built With

* [PHP 7](https://php.net) - Programming language
* [Laravel 5.6 framework](https://laravel.com) - The web framework used 
* [Passport](https://github.com/laravel/passport) - Dependency Management

## Contributing

Please read for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [E-syndic 1.0.0](https://syndic-beta.marocgeek.com) for versioning. For the versions available,

## Authors

* **Abdelhadi Habchi** - *Initial work & FullStack developer* - [AbdelhadiDev](https://abdelhadidev.com)

## License

This project is licensed under the MIT License
