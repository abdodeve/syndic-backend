<?php

namespace Tests;

use Spatie\Activitylog\Models\Activity ;
use Illuminate\Support\Facades\DB;


/**
 * A Custom Debugger Class
 *
 */
class test
{

    /**
     * A Debugger Methode
     *
     */
    public static function testFunction()
    {
        // Use Tenant Connection
        \Config::set('database.default', 'tenant');

        DB::table('groupe_permissions')->delete();
        DB::table('permissions')->delete();

        // Insert default groupe_permissions
        $groupes =  [
            [
                'id'          => 1,
                'name'        => "generale",
                'description' => "Générale"
            ],
            [
                'id'          => 2,
                'name'        => "module_comptabilite",
                'description' => "Module comptabilite"
            ],
            [
                'id'          => 3,
                'name'        => "super-admin-groupe",
                'description' => 'super-admin-groupe'
            ],
            [
                'id'          => 4,
                'name'        => "proprietaire",
                'description' => 'Proprietaire'
            ],
            [
                'id'          => 5,
                'name'        => "propriete",
                'description' => 'Propriete'
            ],
            [
                'id'          => 6,
                'name'        => "propriete_proprietaire",
                'description' => 'ProprieteProprietaire'
            ],
            [
                'id'          => 7,
                'name'        => "copropriete_primaire",
                'description' => 'CoproprietePrimaire'
            ],
        ];
        DB::table('groupe_permissions')->insert($groupes);


        // permissions - Insert default
        $permissions =  [
            /*|--------------|
              | generale     |
              |--------------|*/
            [
                'name'                      => "super-admin-permission",
                'description'               => "Super admin",
                'fk_groupe_permissions'  => null,
            ],
            [
                'name'                      => "admin-permission",
                'description'               => "Administrer",
                'fk_groupe_permissions'  => 1,
            ],
            [
                'name'                      => "restore",
                'description'               => "Restauration",
                'fk_groupe_permissions'  => 1,
            ],
            /*|--------------|
              | proprietaire |
              |--------------|*/
            [
                'name'                      => "display_proprietaire",
                'description'               => "Afficher proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "create_proprietaire",
                'description'               => "Créer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "update_proprietaire",
                'description'               => "Modifier proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "delete_proprietaire",
                'description'               => "Supprimer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "display_proprietaire",
                'description'               => "Créer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "update_proprietaire",
                'description'               => "Modifier proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            [
                'name'                      => "delete_proprietaire",
                'description'               => "Supprimer proprietaire",
                'fk_groupe_permissions'  => 4,
            ],
            /*|--------------|
              | propriete    |
              |--------------|*/
            [
                'name'                      => "display_propriete",
                'description'               => "Afficher propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "create_propriete",
                'description'               => "Créer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "update_propriete",
                'description'               => "Modifier propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "delete_propriete",
                'description'               => "Supprimer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "display_propriete",
                'description'               => "Créer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "update_propriete",
                'description'               => "Modifier propriete",
                'fk_groupe_permissions'  => 5,
            ],
            [
                'name'                      => "delete_propriete",
                'description'               => "Supprimer propriete",
                'fk_groupe_permissions'  => 5,
            ],
            /*|---------------------------|
              | propriete_proprietaire    |
              |---------------------------|*/
            [
                'name'                      => "display_propriete_proprietaire",
                'description'               => "Afficher ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "create_propriete_proprietaire",
                'description'               => "Créer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "update_propriete_proprietaire",
                'description'               => "Modifier ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "delete_propriete_proprietaire",
                'description'               => "Supprimer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "display_propriete_proprietaire",
                'description'               => "Créer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "update_propriete_proprietaire",
                'description'               => "Modifier ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],
            [
                'name'                      => "delete_propriete_proprietaire",
                'description'               => "Supprimer ProprieteProprietaire",
                'fk_groupe_permissions'  => 6,
            ],

            /*|---------------------------|
              | copropriete_primaire      |
              |---------------------------|*/
            [
                'name'                      => "display_copropriete_primaire",
                'description'               => "Afficher CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "create_copropriete_primaire",
                'description'               => "Créer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "update_copropriete_primaire",
                'description'               => "Modifier CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "delete_copropriete_primaire",
                'description'               => "Supprimer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "display_copropriete_primaire",
                'description'               => "Créer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "update_copropriete_primaire",
                'description'               => "Modifier CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],
            [
                'name'                      => "delete_copropriete_primaire",
                'description'               => "Supprimer CoproprietePrimaire",
                'fk_groupe_permissions'  => 7,
            ],


        ];
        DB::table('permissions')->insert($permissions);
    }

    /**
     * A Debugger Methode
     *
     */
    public static function createActivity()
    {
        the_activity('lorem description',
                     1,
                     'copropriete',
                     'GET',
                     '/route/user') ;


       $res2 = self::testFunction() ;

       return [$res2, $res2] ;
    }

    public  static function test() {
       $p = [1,2,3];

        return $p ;
    }
}
