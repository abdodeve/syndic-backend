<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Activitylog\Models\Activity ;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return json
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);

        activity()
            ->causedBy('1')
            ->withProperties(['customProperty' => 'customValue'])
            ->log('Look, I logged something');

        $res = Activity::all();
        $res=$res[0] ;

        echo json_encode($res) ;
    }
}
