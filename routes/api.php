<?php

use Illuminate\Http\Request;
// use PDF;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Teeeeeeeeeeeest Tenancy
Route::post('test1', function (Request $request) {
    return response()->json(['success' => true]);
});

Route::get('generate-pdf', function (Request $request) {
    $data = ['title' => 'Welcome to HDTuto.com'];
    $pdf = PDF::loadView('pdf/myPDF', $data);

    // return $pdf->stream();
   return $pdf->download('itsolutionstuff.pdf');
});

    /*
    |--------------------------------------------------------------------------
    |  Offiline Routes
    |--------------------------------------------------------------------------
    */

    /***********************
     * User Log
     ***********************/
     Route::post('login','UserController@login');
     Route::post('user/logout','UserController@userLogout');
     Route::post('forgot/password','UserController@forgotPassword');
     Route::post('signUp','UserController@signUp');

    /*
    |--------------------------------------------------------------------------
    |  LogedIn Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => ['auth:api', 'tenant']], function () {

        /*
        |--------------------------------------------------------------------------
        |  TEST
        |--------------------------------------------------------------------------
        */
         Route::post('myTest', function(Request $request){
             return App\Models\PieceJointeModel::fetch('facture', 1);
         });



        /*
        |--------------------------------------------------------------------------
        |  User Routes
        |--------------------------------------------------------------------------
        */

        //User CRUD
        Route::post('user/fetch', 'UserController@fetch');
        Route::get('user/single/{id}', 'UserController@single');
        Route::post('user/insert', 'UserController@insert');
        Route::put('user/update/{id}', 'UserController@update');
        Route::post('user/deletePermanently', 'UserController@deletePermanently');
        //User Operations
        Route::get('user/loggedIn', 'UserController@userLoggedIn');
        Route::post('changePassword', 'UserController@changePassword');
        //User Import/Export
        Route::post('user/import', 'UserController@import');
        Route::get('user/export', 'UserController@export');


        /*
        |--------------------------------------------------------------------------
        |  RolePermission Routes
        |--------------------------------------------------------------------------
        */
         Route::post('RolePermesssion/fetch', 'RolePermesssion\RolePermissionController@permissionsRole');
         Route::post('RolePermesssion/getRolesGroupes', 'RolePermesssion\RolePermissionController@getRolesGroupes');
         Route::post('RolePermesssion/assign_permissions_to_role', 'RolePermesssion\RolePermissionController@assign_permissions_to_role');
         Route::post('RolePermesssion/addRole', 'RolePermesssion\RolePermissionController@addRole');
         Route::post('RolePermesssion/updateRole', 'RolePermesssion\RolePermissionController@updateRole');
         Route::post('RolePermesssion/deleteRole', 'RolePermesssion\RolePermissionController@deleteRole');
         Route::post('RolePermesssion/getRoles', 'RolePermesssion\RolePermissionController@getRoles');
         Route::post('RolePermesssion/getPermissions', 'RolePermesssion\RolePermissionController@getPermissions');

         /*
         |--------------------------------------------------------------------------
         |  RolePermission Routes
         |--------------------------------------------------------------------------
         */
         Route::post('activity_log/fetch', 'ActivityLog\ActivityLogController@fetch');



        /*
        |--------------------------------------------------------------------------
        |  Proprietaire Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_proprietaire']], function () {
            Route::post('proprietaire/fetch', 'Proprietaire\ProprietaireController@fetch');
            Route::get('proprietaire/single/{id}', 'Proprietaire\ProprietaireController@single');
        });
        Route::post('proprietaire/insert','Proprietaire\ProprietaireController@insert')->middleware('permissions:create_proprietaire');
        Route::put('proprietaire/update/{id}', 'Proprietaire\ProprietaireController@update')->middleware('permissions:update_proprietaire');
        Route::post('proprietaire/delete', 'Proprietaire\ProprietaireController@delete')->middleware('permissions:delete_proprietaire');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('proprietaire/fetchDeleted', 'Proprietaire\ProprietaireController@fetchDeleted');
            Route::post('proprietaire/restore', 'Proprietaire\ProprietaireController@restore');
            Route::post('proprietaire/deletePermanently', 'Proprietaire\ProprietaireController@deletePermanently');
        });

        /***********************
         * Import/Export
         ***********************/
        Route::get('proprietaire/export', 'Proprietaire\ProprietaireController@export');
        Route::post('proprietaire/import', 'Proprietaire\ProprietaireController@import');

        /***********************
         * Generate PDF
         ***********************/
        Route::get('proprietaire/generatePDF', 'Proprietaire\ProprietaireController@generatePDF');


         /*
        |--------------------------------------------------------------------------
        |  Propriete Routes
        |--------------------------------------------------------------------------
        */
        /***********************
          * primary
          ***********************/
        Route::group(['middleware' => ['permissions:display_propriete']], function () {
             Route::post('propriete/fetch', 'Propriete\ProprieteController@fetch');
             Route::get('propriete/single/{id}', 'Propriete\ProprieteController@single');
        });
        Route::post('propriete/insert','Propriete\ProprieteController@insert')->middleware('permissions:create_propriete');
        Route::put('propriete/update/{id}', 'Propriete\ProprieteController@update')->middleware('permissions:update_propriete');
        Route::post('propriete/delete', 'Propriete\ProprieteController@delete')->middleware('permissions:delete_propriete');
        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('propriete/fetchDeleted', 'Propriete\ProprieteController@fetchDeleted');
            Route::post('propriete/restore', 'Propriete\ProprieteController@restore');
            Route::post('propriete/deletePermanently', 'Propriete\ProprieteController@deletePermanently');
        });

        /*
        |--------------------------------------------------------------------------
        |  ProprieteProprietaire Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_propriete_proprietaire']], function () {
            Route::post('ProprieteProprietaire/fetch', 'ProprieteProprietaire\ProprieteProprietaireController@fetch');
            Route::get('ProprieteProprietaire/single/{id}', 'ProprieteProprietaire\ProprieteProprietaireController@single');
        });
        Route::post('ProprieteProprietaire/insert','ProprieteProprietaire\ProprieteProprietaireController@insert')->middleware('permissions:create_propriete_proprietaire');
        Route::put('ProprieteProprietaire/update/{id}', 'ProprieteProprietaire\ProprieteProprietaireController@update')->middleware('permissions:update_propriete_proprietaire');
        Route::post('ProprieteProprietaire/delete', 'ProprieteProprietaire\ProprieteProprietaireController@delete')->middleware('permissions:delete_propriete_proprietaire');
        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('ProprieteProprietaire/fetchDeleted', 'ProprieteProprietaire\ProprieteProprietaireController@fetchDeleted');
            Route::post('ProprieteProprietaire/restore', 'ProprieteProprietaire\ProprieteProprietaireController@restore');
            Route::post('ProprieteProprietaire/deletePermanently', 'ProprieteProprietaire\ProprieteProprietaireController@deletePermanently');
        });
        /***********************
         * Routes used in Propriete / Proprietaire
         ***********************/
        Route::post('ProprieteProprietaire/getProprietes', 'ProprieteProprietaire\ProprieteProprietaireController@getProprietes');
        Route::post('ProprieteProprietaire/getProprietaires', 'ProprieteProprietaire\ProprieteProprietaireController@getProprietaires');
        Route::post('ProprieteProprietaire/assignProprietesToProprietaire', 'ProprieteProprietaire\ProprieteProprietaireController@assignProprietesToProprietaire');
        Route::post('ProprieteProprietaire/assignProprietairesToPropriete', 'ProprieteProprietaire\ProprieteProprietaireController@assignProprietairesToPropriete');


      /*
      |--------------------------------------------------------------------------
      |  CoproprietePrimaire Routes
      |--------------------------------------------------------------------------
      */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_copropriete_primaire']], function () {
            Route::post('CoproprietePrimaire/fetch', 'CoproprietePrimaire\CoproprietePrimaireController@fetch');
            Route::get('CoproprietePrimaire/single/{id}', 'CoproprietePrimaire\CoproprietePrimaireController@single');
        });
        Route::post('CoproprietePrimaire/insert','CoproprietePrimaire\CoproprietePrimaireController@insert')->middleware('permissions:create_copropriete_primaire');
        Route::put('CoproprietePrimaire/update/{id}', 'CoproprietePrimaire\CoproprietePrimaireController@update')->middleware('permissions:update_copropriete_primaire');
        Route::post('CoproprietePrimaire/delete', 'CoproprietePrimaire\CoproprietePrimaireController@delete')->middleware('permissions:delete_copropriete_primaire');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('CoproprietePrimaire/fetchDeleted', 'CoproprietePrimaire\CoproprietePrimaireController@fetchDeleted');
            Route::post('CoproprietePrimaire/restore', 'CoproprietePrimaire\CoproprietePrimaireController@restore');
            Route::post('CoproprietePrimaire/deletePermanently', 'CoproprietePrimaire\CoproprietePrimaireController@deletePermanently');
        });

      /*
      |--------------------------------------------------------------------------
      |  Copropriete Routes
      |--------------------------------------------------------------------------
      */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_copropriete']], function () {
            Route::post('copropriete/fetch', 'Copropriete\CoproprieteController@fetch');
            Route::get('copropriete/single/{id}', 'Copropriete\CoproprieteController@single');
        });
        Route::post('copropriete/insert','Copropriete\CoproprieteController@insert')->middleware('permissions:create_copropriete');
        Route::put('copropriete/update/{id}', 'Copropriete\CoproprieteController@update')->middleware('permissions:update_copropriete');
        Route::post('copropriete/delete', 'Copropriete\CoproprieteController@delete')->middleware('permissions:delete_copropriete');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('copropriete/fetchDeleted', 'Copropriete\CoproprieteController@fetchDeleted');
            Route::post('copropriete/restore', 'Copropriete\CoproprieteController@restore');
            Route::post('copropriete/deletePermanently', 'Copropriete\CoproprieteController@deletePermanently');
        });

        Route::get('copropriete/coproprietesExercices','Copropriete\CoproprieteController@getCoproprietesExercices');

        /*
        |--------------------------------------------------------------------------
        |  TypeCopropriete Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_type_copropriete']], function () {
            Route::post('TypeCopropriete/fetch', 'TypeCopropriete\TypeCoproprieteController@fetch');
            Route::get('TypeCopropriete/single/{id}', 'TypeCopropriete\TypeCoproprieteController@single');
        });
        Route::post('TypeCopropriete/insert','TypeCopropriete\TypeCoproprieteController@insert')->middleware('permissions:create_type_copropriete');
        Route::put('TypeCopropriete/update/{id}', 'TypeCopropriete\TypeCoproprieteController@update')->middleware('permissions:update_type_copropriete');
        Route::post('TypeCopropriete/delete', 'TypeCopropriete\TypeCoproprieteController@delete')->middleware('permissions:delete_type_copropriete');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('TypeCopropriete/fetchDeleted', 'TypeCopropriete\TypeCoproprieteController@fetchDeleted');
            Route::post('TypeCopropriete/restore', 'TypeCopropriete\TypeCoproprieteController@restore');
            Route::post('TypeCopropriete/deletePermanently', 'TypeCopropriete\TypeCoproprieteController@deletePermanently');
        });

       /*
       |--------------------------------------------------------------------------
       |  facture Routes
       |--------------------------------------------------------------------------
       */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_facture']], function () {
            Route::post('facture/fetch', 'Facture\FactureController@fetch');
            Route::get('facture/single/{id}', 'Facture\FactureController@single');
        });
        Route::post('facture/insert','Facture\FactureController@insert')->middleware('permissions:create_facture');
        Route::put('facture/update/{id}', 'Facture\FactureController@update')->middleware('permissions:update_facture');
        Route::post('facture/delete', 'Facture\FactureController@delete')->middleware('permissions:delete_facture');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('facture/fetchDeleted', 'Facture\FactureController@fetchDeleted');
            Route::post('facture/restore', 'Facture\FactureController@restore');
            Route::post('facture/deletePermanently', 'Facture\FactureController@deletePermanently');
        });

        /*
        |--------------------------------------------------------------------------
        |  piece_jointe Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_piece_jointe']], function () {
            Route::post('piece_jointe/fetch', 'PieceJointe\PieceJointeController@fetch');
        });
        Route::post('piece_jointe/insert','PieceJointe\PieceJointeController@insert')->middleware('permissions:create_piece_jointe');
        Route::post('piece_jointe/delete', 'PieceJointe\PieceJointeController@deleteSoft')->middleware('permissions:delete_piece_jointe');
        Route::post('piece_jointe/deletePermanently', 'PieceJointe\PieceJointeController@deletePermanently')->middleware('permissions:delete_permanently_piece_jointe');
        Route::get('piece_jointe/download/{piece_id}', 'PieceJointe\PieceJointeController@download')->middleware('permissions:download_piece_jointe');



        /*
        |--------------------------------------------------------------------------
        |  syndic Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_syndic']], function () {
            Route::post('syndic/fetch', 'Syndic\SyndicController@fetch');
            Route::get('syndic/single/{id}', 'Syndic\SyndicController@single');
        });
        Route::post('syndic/insert','Syndic\SyndicController@insert')->middleware('permissions:create_syndic');
        Route::put('syndic/update/{id}', 'Syndic\SyndicController@update')->middleware('permissions:update_syndic');
        Route::post('syndic/delete', 'Syndic\SyndicController@delete')->middleware('permissions:delete_syndic');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('syndic/fetchDeleted', 'Syndic\SyndicController@fetchDeleted');
            Route::post('syndic/restore', 'Syndic\SyndicController@restore');
            Route::post('syndic/deletePermanently', 'Syndic\SyndicController@deletePermanently');
        });



        /*
        |--------------------------------------------------------------------------
        |  banque Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_banque']], function () {
            Route::post('banque/fetch', 'Banque\BanqueController@fetch');
            Route::get('banque/single/{id}', 'Banque\BanqueController@single');
        });
        Route::post('banque/insert','Banque\BanqueController@insert')->middleware('permissions:create_banque');
        Route::put('banque/update/{id}', 'Banque\BanqueController@update')->middleware('permissions:update_banque');
        Route::post('banque/delete', 'Banque\BanqueController@delete')->middleware('permissions:delete_banque');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('banque/fetchDeleted', 'Banque\BanqueController@fetchDeleted');
            Route::post('banque/restore', 'Banque\BanqueController@restore');
            Route::post('banque/deletePermanently', 'Banque\BanqueController@deletePermanently');
        });




        /*
        |--------------------------------------------------------------------------
        |  paiement Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_paiement']], function () {
            Route::post('paiement/fetch', 'Paiement\PaiementController@fetch');
            Route::get('paiement/single/{id}', 'Paiement\PaiementController@single');
        });
        Route::post('paiement/insert','Paiement\PaiementController@insert')->middleware('permissions:create_paiement');
        Route::put('paiement/update/{id}', 'Paiement\PaiementController@update')->middleware('permissions:update_paiement');
        Route::post('paiement/delete', 'Paiement\PaiementController@delete')->middleware('permissions:delete_paiement');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('paiement/fetchDeleted', 'Paiement\PaiementController@fetchDeleted');
            Route::post('paiement/restore', 'Paiement\PaiementController@restore');
            Route::post('paiement/deletePermanently', 'Paiement\PaiementController@deletePermanently');
        });



        /*
        |--------------------------------------------------------------------------
        |  budget Routes
        |--------------------------------------------------------------------------
        */
        /***********************
         * primary
         ***********************/
        Route::group(['middleware' => ['permissions:display_budget']], function () {
            Route::post('budget/fetch', 'Budget\BudgetController@fetch');
            Route::get('budget/single/{id}', 'Budget\BudgetController@single');
        });
        Route::post('budget/insert','Budget\BudgetController@insert')->middleware('permissions:create_budget');
        Route::put('budget/update/{id}', 'Budget\BudgetController@update')->middleware('permissions:update_budget');
        Route::post('budget/delete', 'Budget\BudgetController@delete')->middleware('permissions:delete_budget');

        /***********************
         * restore
         ***********************/
        Route::group(['middleware' => ['permissions:restore']], function () {
            Route::post('budget/fetchDeleted', 'Budget\BudgetController@fetchDeleted');
            Route::post('budget/restore', 'Budget\BudgetController@restore');
            Route::post('budget/deletePermanently', 'Budget\BudgetController@deletePermanently');
        });



    }) ;
