<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model ;
use Illuminate\Support\Facades\DB;

class RolePermissionModel extends Model
{
    /**
     * Get the user's first name.
     *
     * @param  mixed  $values(id_role, id_cat_permission)
     * @return array  permissionsRole
     */
    public static function getPermissionsRoles($values) {
        $prefix = DB::getTablePrefix() ;
        $bindings = ['id_role'=> $values->id_role,
                     'id_cat_permission'=> $values->id_cat_permission] ;

        $sql = "SELECT
                    {$prefix}permissions.id as 'permission_id',
                    {$prefix}permissions.name as 'permission_name',
                    {$prefix}permissions.description as 'permission_description',
                        (CASE WHEN
                            permissionWithRole.role_id IS NULL THEN '0'
                        ELSE '1' END)
                        AS 'role_has_permission'
                FROM
                    {$prefix}permissions
                LEFT JOIN
                (SELECT
                    {$prefix}permissions.id as 'permission_id',
                    {$prefix}permissions.name as 'permission_name',
                    {$prefix}role_has_permissions.role_id as 'role_id'
                  FROM
                    {$prefix}permissions
                        LEFT JOIN
                                {$prefix}role_has_permissions
                            ON
                                {$prefix}permissions.id = {$prefix}role_has_permissions.permission_id
                            WHERE
                                {$prefix}role_has_permissions.role_id = :id_role
                ) permissionWithRole
                ON
                      {$prefix}permissions.id = permissionWithRole.permission_id
                INNER JOIN {$prefix}groupe_permissions
                ON {$prefix}groupe_permissions.id = {$prefix}permissions.fk_groupe_permissions
                WHERE {$prefix}permissions.fk_groupe_permissions = :id_cat_permission
                ORDER BY {$prefix}permissions.id" ;

        $permissionsRole = DB::select($sql, $bindings);

        return $permissionsRole ;
    }

    /**
     * Synchronise role_has_permissions
     *
     * @param mixed $values (role_id, assign, remove)
     * @return array role_has_permissions
     */
    public static function syncRolePermission($values) {
        // Merge assign & remove array
        $assign = $values->assign ? $values->assign : [] ;
        $remove = $values->remove ? $values->remove : [] ;
        $m = array_merge($values->assign ,$values->remove);

       //Delete all affectation for this role
        $res = DB::table('role_has_permissions')
            ->where('role_id','=' , $values->role_id)
            ->whereIn('permission_id', $m)
            ->delete();

        // Affecte new permissions to this role
        $dataSet = [];
        foreach ($assign as $e) {
            $dataSet[] = [
                            'role_id'          => $values->role_id,
                            'permission_id'    => $e
                        ];
        }
        $res = DB::table('role_has_permissions')->insert($dataSet);
        return $res ;
    }

    /**
     * getPermissions - Func Model
     * Get All Permissions of A User
     *
     * @params user_id
     * @return array $permissions
     *
     * */
    public static function getPermissions($request){
        // Retrieve $this User
        $user = \Auth::user() ;
        $super_admin_groupe = 'super-admin-groupe' ;
        $super_admin_role = 'super-admin' ;
        $admin_role = 'admin' ;
        $perm = [] ;
        $enabledModules = RolePermissionModel::enabledModules();
        // Excluded Groupe
         $excluded_group = $enabledModules['grp_of_disabled_modules'] ;
         $excluded_group[] = $super_admin_groupe ;
         $excluded_group = implode("', '", $excluded_group) ;

        // Retrieve role name of this user
        $role_user = RoleModel::getRoleAuth() ;

        switch ($role_user){
            case $super_admin_role: { // Super Admin
                $perm = DB::table('permissions')
                        ->select('permissions.name')
                        ->get();
                break ;
            }
            case $admin_role: { // Admin
                $perm = DB::table('permissions')
                        ->select('permissions.name')
                        ->join('groupe_permissions', 'groupe_permissions.id', '=','permissions.fk_groupe_permissions')
                        ->whereNotIn('groupe_permissions.name', array_merge([$super_admin_groupe], RolePermissionModel::enabledModules()['grp_of_disabled_modules']))
                        ->get();
                break ;
            }
            default: { // Other roles
                // Normale Permissions
            /*    $perm  =   DB::table('users')
                    ->select('permissions.name')
                    ->join('roles', 'roles.id', '=','users.fk_roles')
                    ->join('role_has_permissions', 'role_has_permissions.role_id', '=', 'roles.id')
                    ->join('permissions', 'permissions.id','=', 'role_has_permissions.permission_id')
                    ->join('groupe_permissions', 'groupe_permissions.id', '=','permissions.fk_groupe_permissions')
                    ->where('users.id', '=', $user->id)
                    ->whereNotIn('groupe_permissions.name', $excluded_group)
                   // ->WhereIn('permissions.name', $enabledModules['perms_of_enabled_modules'])
                    ->get()
                    ->toArray();*/

                // Init AppSettings
                $main           = \AppSettings::db_name_main();
                $tenant         = \AppSettings::db_name_tenant();
                $pm             = \AppSettings::prefix_main();
                $pt             = \AppSettings::prefix_tenant();
                $user_id        = \Auth::id() ;

                // Make Select Request
                $bindings = ['id_user'=> $user_id] ;
                $sql     = "select 
                            {$tenant}.{$pt}permissions.name 
                            from {$main}.{$pm}users 
                            inner join {$tenant}.{$pt}roles 
                            on {$tenant}.{$pt}roles.id = {$main}.{$pm}users.fk_roles 
                            inner join {$tenant}.{$pt}role_has_permissions 
                            on {$tenant}.{$pt}role_has_permissions.role_id = {$tenant}.{$pt}roles.id 
                            inner join {$tenant}.{$pt}permissions 
                            on {$tenant}.{$pt}permissions.id = {$tenant}.{$pt}role_has_permissions.permission_id
                            inner join {$tenant}.{$pt}groupe_permissions
                            on {$tenant}.{$pt}groupe_permissions.id = {$tenant}.{$pt}permissions.fk_groupe_permissions 
                            where {$main}.{$pm}users.id = :id_user
                            and {$tenant}.{$pt}groupe_permissions.name not in ('{$excluded_group}')";

                $perm = DB::select($sql, $bindings);

                // Get Permissions Of super-admin-groupe (Just Enabled permissions)
                $perm2 = DB::table('permissions')
                            ->select('permissions.name')
                            ->whereIn('permissions.name', $enabledModules['grp_of_enabled_modules'])
                            ->get()
                            ->toArray();

                // Add Module perms
                $perm = array_merge($perm, $perm2);
                break ;
            }
        } // End Switch

        // Convert $perm from Object Array To Array
        $dataSet = [];
        foreach ($perm as $e) {
            $dataSet[] = $e->name ;
        }

        return $dataSet ;
    }

    /**
     * Retrieve Enabled Modules
     *
     * @return array (
     *                perms_of_enabled_modules,
     *                perms_of_disabled_modules,
     *                grp_of_enabled_modules,
     *                grp_of_disabled_modules
     *               )
     */
    public static function enabledModules(){
        // Modules: permissions under super-admin-groupe
        $app_modules = ['module_comptabilite'=>['module_comptabilite', 'perm_comptabilite_1', 'compt_2'],
                        'some_module'=>['some_1', 'some_2']
                       ] ;
        $perms_of_enabled_modules = [] ;
        $perms_of_disabled_modules = [] ;
        $grp_of_enabled_modules = [] ;
        $grp_of_disabled_modules = [] ;
        // Check if modules are enabled
        foreach ($app_modules as $key => $value){
            $check = DB::table('permissions')
                        ->join('role_has_permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                        ->join('roles', 'roles.id', '=', 'role_has_permissions.role_id')
                        ->where('permissions.name', '=', $key)
                        ->count() ;
            if($check > 0) {
                $perms_of_enabled_modules = $value;
                if(!in_array($key, $grp_of_enabled_modules))
                    $grp_of_enabled_modules[] = $key ;
            }
            else {
                $perms_of_disabled_modules = $value;
                if(!in_array($key, $grp_of_disabled_modules))
                    $grp_of_disabled_modules[] = $key ;
            }

        } // End ForEach

        return ['perms_of_enabled_modules'=> $perms_of_enabled_modules,
                'perms_of_disabled_modules'=> $perms_of_disabled_modules,
                'grp_of_enabled_modules'=> $grp_of_enabled_modules,
                'grp_of_disabled_modules'=> $grp_of_disabled_modules] ;
    }
}