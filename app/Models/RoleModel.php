<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoleModel extends Model
{
    protected $table = 'roles';
    protected $connection = 'tenant' ;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public static function getRoleAuth() {
        // Init AppSettings
        $main           = \AppSettings::db_name_main();
        $tenant         = \AppSettings::db_name_tenant();
        $pm             = \AppSettings::prefix_main();
        $pt             = \AppSettings::prefix_tenant();
        $clients_app_id = \AppSettings::clients_app_id();
        $user_id        = \Auth::id() ;

        // Make Select Request
        $bindings = ['id_user'=> $user_id] ;
        $sql =     "SELECT {$tenant}.{$pt}roles.name
                    FROM {$main}.{$pm}users
                    INNER JOIN {$tenant}.{$pt}roles
                    ON {$tenant}.{$pt}roles.id = {$main}.{$pm}users.fk_roles
                    WHERE {$main}.{$pm}users.id = :id_user" ;

        $role_name = DB::select($sql, $bindings);

        return $role_name[0]->name ;
    }

}
