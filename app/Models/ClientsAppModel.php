<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string db_host
 * @property string db_name
 * @property string db_username
 * @property string db_password
 */
class ClientsAppModel extends Model
{

    protected $table   = 'clients_app';

    /**
     * Get DatabaseName of Tenant Database
     *
     * @return string - Database Tenant Name
     */
    public static function TenantName () {

        // Get clients_app ID from user table
        $id_clients = \Auth::user()->fk_clients_app ;

        $db_name_tenant = \DB::connection('main')
                            ->table('clients_app')
                            ->select('db_name')
                            ->where('id', $id_clients)
                            ->get()
                            ->first() ;
        return $db_name_tenant->db_name ;
    }

    public static function test(){
        \Artisan::call('config:clear');
    }
}
