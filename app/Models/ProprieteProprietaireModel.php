<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProprieteProprietaireModel extends Model
{
    protected $table    = 'propriete_proprietaire';

    /**
     * isDuplicate
     *
     * Check if ProprieteProprietaire duplicated or not
     *
     * @param array $values([propriete_id, proprietaire_id])
     *
     * True  : if ProprieteProprietaire duplicate
     * False : if ProprieteProprietaire not duplicate
     *
     * @return bool
     */
    public static function isDuplicate($values){
        $isDuplicate = \DB::table('propriete_proprietaire')
                            ->where('fk_propriete', $values['propriete_id'])
                            ->where('fk_proprietaire', $values['proprietaire_id'])
                            ->exists() ;
        $isDuplicate = $isDuplicate ? true : false ;

        return $isDuplicate ;
    }
}
