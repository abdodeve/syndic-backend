<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProprieteModel extends Model
{
    protected $table    = 'propriete' ;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nom'];
}
