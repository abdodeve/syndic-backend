<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FactureModel extends Model
{
    protected $table    = 'facture';
}
