<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SyndicModel extends Model
{
    protected $table    = 'syndic';
}
