<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoproprieteModel extends Model
{
    protected $table          = 'copropriete';
}
