<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class PieceJointeModel extends Model
{

    protected $connection = 'tenant' ;

    /**
     * @property int           $id
     * @property string        $name
     * @property string        $source
     * @property string        $type
     *
     */

    #  Important !
    /**
     *  $type - This is possible values of $type
     * #########################################
     *
     *  #1 copropriete_images
    */

    protected $table    = 'piece_jointe';


    /**
     * Retrieve PieceJointes Of a Table passed by parameter
     *
     * @param $tableToJoin - (Table name to Join with PieceJoint)
     * @param $tableToJoin_id - (ID row of tableJoined)
     * @param $type - type Of PieceJointe
     *
     * @return mixed $piece_jointes
     *
     */
    public static function fetch($tableToJoin, $tableToJoin_id, $type=null){

        if(!$tableToJoin || !$tableToJoin_id) return 'tableToJoin & tableToJoin_id are Requiered' ;

        // Retrieve data from piece_jointe Table
        $piece_jointes = \DB::table('piece_jointe')
                            ->join($tableToJoin,
                                $tableToJoin.'.id',
                                '=',
                                'piece_jointe.fk_'.$tableToJoin
                            )
                            ->where($tableToJoin.'.id', $tableToJoin_id)
                            ->where('piece_jointe.status', '=', 'active')
                            ->when($type, function($query) use ($type) {
                                $query->where('type', '=', $type);
                            })
                            ->select('piece_jointe.*')
                            ->orderBy('piece_jointe.id', 'desc')
                            ->get();

        return $piece_jointes;
    }


    /**
     * Delete PieceJointe - Change Status from Active to Deleted
     *
     * @param array $piece_jointes_ids
     * @param $type - type Of PieceJointe
     *
     * @return mixed $piece_jointes
     *
     */
    public static function deleteSoft($piece_jointes_ids, $type = null){


        if(empty($piece_jointes_ids)) return false ;

        // Change status to deleted
        \DB::table('piece_jointe')
            ->whereIn ('id', $piece_jointes_ids)
            ->when($type, function($query) use ($type) {
                $query->where('type', '=', $type);
            })
            ->update(['status'=> 'deleted']);

        return $piece_jointes_ids ;
    }


    /**
     * Delete PieceJointe Permanently
     *
     * @param $piece_jointes_ids
     * @param $type - type Of PieceJointe
     *
     * @return mixed
     *
     */
    public static function deletePermanently($piece_jointes_ids, $type = null){

        \Config::set('database.default', 'tenant');

        // Check if empty
        if(empty($piece_jointes_ids)) return false ;

        /*******************************************
         * Delete Files
         *******************************************/
        // Get sources
        $srcs = \DB::table('piece_jointe')
            ->select('piece_jointe.source')
            ->whereIn ('id', $piece_jointes_ids)
            ->when($type, function($query) use ($type) {
                $query->where('type', '=', $type);
            })
            ->get();

        $sources = [] ;
        foreach ($srcs as $source){
            $sources[] = $source->source ;
        }

        // Delete Files
        $res = Storage::delete($sources);

        // Destroy PieceJointes
        $deletedRows = PieceJointeModel::destroy($piece_jointes_ids);

        return $deletedRows>0 ? $piece_jointes_ids : false ;
    }



    /**
     * Restore PieceJointe
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public static function restore($piece_jointes_ids){

        // Check if empty
        if(empty($piece_jointes_ids)) return false ;

        // Change status to activr
        $updatedRows = \DB::table('piece_jointe')
                            ->whereIn ('id', $piece_jointes_ids)
                            ->update(['status'=> 'active']);

        return $updatedRows>0 ? $piece_jointes_ids : false ;
    }


    /**
     * Get PieceJointe IDs
     *
     * @param string $tableToJoin
     * @param array $table_ids
     * @param $type - type Of PieceJointe
     *
     * @return mixed $pieces
     *
     */
    public static function getPiecesIds($tableToJoin, $table_ids, $type=null){

        $piece_jointes = \DB::table('piece_jointe')
                            ->whereIn('fk_'.$tableToJoin, $table_ids)
                            ->when($type, function($query) use ($type) {
                                $query->where('type', '=', $type);
                            })
                            ->get(['id']);

        $pieces = null ;
        foreach ($piece_jointes as $piece){
            $pieces[] = $piece->id ;
        }

        return $pieces ;
    }

}
