<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeCoproprieteModel extends Model
{
    protected $table    = 'type_copropriete';
}
