<?php

namespace App\Imports;

use App\Models\ProprietaireModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class ProprietaireImport implements ToCollection, WithHeadingRow
{
    public $importedObjects = [] ;

    /**
     * @param array $row
     *
     * @return Proprietaire|null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {

            $proprietaire = new ProprietaireModel();
            $proprietaire->fk_copropriete   = $row['fk_copropriete'];
            $proprietaire->nom              = $row['nom'];
            $proprietaire->prenom           = $row['prenom'] ;
            $proprietaire->cin              = $row['cin'] ;
            $proprietaire->profession       = $row['profession'] ;
            $proprietaire->titre            = $row['titre'] ;
            $proprietaire->ville            = $row['ville'] ;
            $proprietaire->adresse_1        = $row['adresse_1'] ;
            $proprietaire->adresse_2        = $row['adresse_2'] ;
            $proprietaire->email_1          = $row['email_1'] ;
            $proprietaire->email_2          = $row['email_2'] ;
            $proprietaire->tel_1            = $row['tel_1'] ;
            $proprietaire->tel_2            = $row['tel_1'] ;
            $proprietaire->code_postale     = $row['tel_2'];
            $proprietaire->description      = $row['code_postale'] ;
            $proprietaire->status           = 'active' ;
    
            $this->importedObjects[] = $proprietaire ;
            $proprietaire->save();
        }
    }
}