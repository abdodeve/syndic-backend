<?php

namespace App\Helpers ;

class AppSettings {

    /**
     * Get prefix of databases Main
     *
     * @return string
     */
   public static function prefix_main() {
       $prefix_main = \DB::connection('main')->getTablePrefix() ;
       return $prefix_main ;
    }

    /**
     * Get prefix of databases Tenant
     *
     * @return string
     */
    public static function prefix_tenant() {
        $prefix_tenant = \DB::connection('tenant')->getTablePrefix() ;
        return $prefix_tenant ;
    }

    /**
     * Get clients_app ID from user table
     *
     * @return mixed
     */
    public static function clients_app_id() {
        $id_clients = \Auth::user()->fk_clients_app ;
        return $id_clients ;
    }

    /**
     * Get DatabaseName of Main Database
     *
     * @return mixed
     */
    public static function db_name_main() {
        $db_name_main   = \Config::get('database.connections.main.database');
        return $db_name_main ;
    }

    /**
     * Get DatabaseName of Tenant Database
     *
     * @return mixed
     */
    public static function db_name_tenant() {
        $db_name_tenant = \DB::connection('main')
                            ->table('clients_app')
                            ->select('db_name')
                            ->where('id', self::clients_app_id())
                            ->get()
                            ->first();
        $db_name_tenant = $db_name_tenant->db_name ;
        return $db_name_tenant ;
    }

}