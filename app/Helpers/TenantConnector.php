<?php

namespace App\Helpers;

use App\Models\ClientsAppModel ;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TenantConnector {

    /**
     * Switch the Tenant connection .
     * @param Int $id - ClientsApp ID
     * @return mixed
     * @throws
     */
    public static function reconnect($id = null) {
        $clients_app = null ;

        // Erase the tenant connection, thus making Laravel get the default values all over again.
        DB::purge('main');
        DB::purge('tenant');
        \Artisan::call('config:clear');

        // If no $id given connect to main Connection
        if($id == null) {
            // Switch to main connection
            Config::set('database.default', 'main');
            return 'no id clients_app given (reconnect to main)';
        }

        // Else if $id given
        $clients_app = ClientsAppModel::findOrFail($id);

        // Set Connection Credientials
        if($clients_app->db_host)
        Config::set('database.connections.tenant.host'   ,  $clients_app->db_host);
        if($clients_app->db_name)
        Config::set('database.connections.tenant.database', $clients_app->db_name);
        if($clients_app->db_username)
        Config::set('database.connections.tenant.username', $clients_app->db_username);
        if($clients_app->db_password)
        Config::set('database.connections.tenant.password', $clients_app->db_password);

        // Switch to a tenant connection
        Config::set('database.default', 'tenant');

        // Ping the database. This will throw an exception in case the database does not exists or the connection fails
        Schema::connection('tenant')->getConnection()->reconnect();
        return "id clients_app = {$id} given (reconnect to {$clients_app->db_name})";
    }

}