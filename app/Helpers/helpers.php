<?php

use App\Events\EventGlobale ;

//if (! function_exists('abc_1')) {

    function abc_1()
    {
        return 'abc - 123';
    }
//}

if (! function_exists('the_activity')) {
    /**
     * Create activity_log
     *
     * @param string $description
     * @param int $user_id
     * @param string $table
     * @param string $methode
     * @param mixed $route
     *
     * @return mixed activity_log
     */
    function the_activity($description, $table = null) {

         // Insert Activity
         $activity = activity()
                        ->withProperties([
                            'table'          => $table,
                            'date'           => (string) \Carbon\Carbon::now(),
                            'methode'        => \Request::getMethod(),
                            'route'          => \Request::path(),
                            'ip_address'     => \Request::ip()
                        ])
                        ->log($description);

         // Retrieve Inserted Activity
        $id = $activity->id ;
        $user_id = $activity->causer->id;
        $user_name = $activity->causer->name;
        $description = $activity->description;
        $table = $activity->getExtraProperty('table');
        $date = $activity->getExtraProperty('date');
        $methode = $activity->getExtraProperty('methode');
        $route = $activity->getExtraProperty('route');
        $ip_address = $activity->getExtraProperty('ip_address');

        $activity_ret = [
            'id' => $id,
            'user_id' => $user_id,
            'user_name' => $user_name,
            'description' => $description,
            'table' => $table,
            'date' => $date,
            'methode' => $methode,
            'route' => $route,
            'ip_address' => $ip_address
        ];
           
         return $activity ;
        }
    }

    if (! function_exists('reconnect')) {
        /**
         * Switch the Tenant connection .
         * @param Int $id - ClientsApp ID
         * @return mixed
         * @throws
         */
        function reconnect($id = null)
        {
           return App\Helpers\TenantConnector::reconnect($id);
        }
    }
