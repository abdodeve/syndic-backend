<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SignUpEmailToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $passedData ;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($passedData)
    {
        $this->passedData = $passedData ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@marocgeek.com')
                    ->subject("You have created a new account at E-Syndic")
                    ->view('email.signup_email_to_user');
    }
}
