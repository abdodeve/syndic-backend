<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $passedData ;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($passedData)
    {
        $this->passedData = $passedData ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@marocgeek.com')
                    ->subject("Your new password generated E-Syndic")
                    ->view('email.forgot_password_email');
    }
}
