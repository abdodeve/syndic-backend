<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class cleanTrash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanTrash:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears out all rows have deleted as status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('proprietaire')
            ->where('status', '=', 'deleted')
            ->where(function($q){
                $q->where(DB::raw("time_to_sec(timediff( NOW(), updated_at)) / 3600"), ">", "24")
                  ->orWhere('updated_at','=', NULL);
            })
            ->delete();
    }
}
