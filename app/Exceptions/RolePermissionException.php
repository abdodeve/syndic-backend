<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class RolePermissionException extends HttpException
{

    public static function permissionException($permissions): self
    {
         // Array to string
         $p = implode(" , ", $permissions) ;
         $message = 'Access Denied - This feature require the following permissions : '.$p;

         $exception = new static(403, $message, null, []);
         return $exception;
    }
}