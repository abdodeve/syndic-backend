<?php

namespace App\Exports;

use App\Models\ProprietaireModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ProprietaireExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ProprietaireModel::all();
    }

    public function headings(): array
    {
        return [
            'id',
            'fk_copropriete',
            'nom',
            'prenom',
            'cin',
            'profession',
            'titre',
            'ville',
            'adresse_1',
            'adresse_2',
            'email_1',
            'email_2',
            'tel_1',
            'tel_2',
            'code_postale',
            'description',
            'status',
            'created_at',
            'updated_at'
        ];
    }
}
