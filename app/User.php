<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens ;

    protected $connection = 'main' ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $guard_name = 'api';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     *  Fetch All Users
     *
     *  @return array
     */
    public static function fetch() {

        // Init AppSettings
        $pm             = \AppSettings::prefix_main();
        $pt             = \AppSettings::prefix_tenant();
        $clients_app_id = \AppSettings::clients_app_id();
        $main           = \AppSettings::db_name_main();
        $tenant         = \AppSettings::db_name_tenant();

        // Make Select Request
        $sql = "SELECT 
                    {$main}.{$pm}users.id,
                    {$main}.{$pm}users.name,
                    {$main}.{$pm}users.email,
                    {$tenant}.{$pt}roles.name as role_name
                FROM {$main}.{$pm}users
                LEFT JOIN {$tenant}.{$pt}roles
                ON {$tenant}.{$pt}roles.id = {$main}.{$pm}users.fk_roles
                WHERE {$main}.{$pm}users.fk_clients_app = {$clients_app_id}
                AND   {$main}.{$pm}users.active = 1
                ORDER BY {$main}.{$pm}users.name ASC" ;

        $users = \DB::select($sql);

        return $users ;
    }

    public function testFunc(){

        return this.id ;
    }



    /**
     * Delete all permissions assigned
     * table : model_has_permissions
     *
     * @param $id user id
     * @return mixed - user_id deleted
     */
    protected function unbindPermissions($user_id) {
        DB::table('model_has_permissions')
            ->where('model_id', '=', $user_id)
            ->delete();
        return ['id'=> $user_id] ;
    }



    /**
     * Get all permissions of a role
     * table : role_has_permission
     *
     * @param $role_id
     * @return $permissions array
     */
    protected function getPermissionsOfRole($role_id) {
        $permissions =  DB::table('role_has_permissions')
                        ->select('permission_id')
                        ->where('role_id', '=', $role_id)
                        ->get();

        $p = [] ;
        foreach ($permissions as $e) {
            $p[] = $e->permission_id ;
        }

        return $p ;
    }




    /**
     * Unbind & Assign permissions to user
     * table : model_has_permissions
     *
     * @param $role_id
     * @return $permissions array
     */
    public function assign_permissions_to_model($role_id) {
        if (!$role_id) return ;

        // Retrieve $this User
        $the_user = (object) get_object_vars($this) ;
        $user_attr = (object) $the_user->attributes ;

        $p = $this->getPermissionsOfRole($role_id);
        $this->unbindPermissions($user_attr->id);
        $this->givePermissionTo($p);
    }




    /**
     *  Check if this User
     *  Has given Permission
     *
     *  @param $permission
     *  @return bool
     */
    public function havePermission($permission) {

        // Retrieve $this User
        $the_user = (object) get_object_vars($this) ;
        $user_attr = (object) $the_user->attributes ;

        $super_admin_groupe = 'super-admin-groupe' ;
        $super_admin_role = 'super-admin' ;
        $admin_role = 'admin' ;

        // Retrieve role name of this user
        $role_user = Models\RoleModel::getRoleAuth() ;

        switch ($role_user){
            case $super_admin_role: {
                     return true ;
                     break ;
                }
            case $admin_role: {
                $check_permissions = DB::table('permissions')
                                       ->join('groupe_permissions',
                                               'groupe_permissions.id',
                                               '=',
                                                'permissions.fk_groupe_permissions')
                                       ->where('groupe_permissions.name', '<>', $super_admin_groupe)
                                       ->where('permissions.name', '=', $permission)
                                       ->count();
                    return ($check_permissions > 0) ? true : false ;
                    break ;
                }
            default: {

                $permission = implode("', '", $permission) ;

                // Init AppSettings
                $main           = \AppSettings::db_name_main();
                $tenant         = \AppSettings::db_name_tenant();
                $pm             = \AppSettings::prefix_main();
                $pt             = \AppSettings::prefix_tenant();
                $user_id        = \Auth::id();

                // Make Select Request
                $bindings = ['id_user'=> $user_id] ;
                $sql = "SELECT 
                          COUNT(*) AS AGGREGATE
                        FROM {$main}.{$pm}users
                        INNER JOIN {$tenant}.{$pt}roles
                        ON {$tenant}.{$pt}roles.id = {$main}.{$pm}users.fk_roles
                        INNER JOIN {$tenant}.{$pt}role_has_permissions
                        ON {$tenant}.{$pt}role_has_permissions.role_id = {$tenant}.{$pt}roles.id 
                        INNER JOIN {$tenant}.{$pt}permissions
                        ON {$tenant}.{$pt}permissions.id = {$tenant}.{$pt}role_has_permissions.permission_id 
                        WHERE
                         {$main}.{$pm}users.id = :id_user
                        AND
                         {$tenant}.{$pt}permissions.name in ('{$permission}')" ;

                $check_permissions = \DB::select($sql, $bindings);


               /* $check_permissions  =   DB::table('users')
                    ->join('roles', 'roles.id', '=','users.fk_roles')
                    ->join('role_has_permissions', 'role_has_permissions.role_id', '=', 'roles.id')
                    ->join('permissions', 'permissions.id','=', 'role_has_permissions.permission_id')
                    ->where('users.id', '=', $user_attr->id)
                    ->whereIn('permissions.name', $permission)
                    ->count() ;*/
                return ($check_permissions > 0) ? true : false ;
                break ;
            }
        } // End Switch

    } // End Methode





    /**
     *  Check if this User
     *  Is Super Admin
     *
     *  @return bool
     */
    public function isSuperAdmin(){

        // Init AppSettings
        $main           = \AppSettings::db_name_main();
        $tenant         = \AppSettings::db_name_tenant();
        $pm             = \AppSettings::prefix_main();
        $pt             = \AppSettings::prefix_tenant();
        $clients_app_id = \AppSettings::clients_app_id();

        // Make Select Request
        $sql = "SELECT
                    count(*) as aggregate 
                FROM 
                    {$main}.{$pm}users
                INNER JOIN 
                    {$tenant}.{$pt}roles
                ON 
                    {$tenant}.{$pt}roles.id = {$main}.{$pm}users.fk_roles 
                WHERE 
                    {$main}.{$pm}users.id = 1
                AND 
                    {$tenant}.{$pt}roles.name = 'super-admin'
                AND 
                    {$main}.{$pm}users.fk_clients_app = {$clients_app_id}";

        // Check if this user have Super Role
        $check_role = \DB::select($sql);

         return ($check_role > 0) ? true : false ;
    }

    public function vr_test() {
        $vr1 = '123' ; // DB::connection('main')->getTablePrefix() ;
        return $vr1 ;
    }
  
}
