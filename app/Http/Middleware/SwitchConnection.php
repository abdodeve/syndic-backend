<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ClientsAppModel ;

class SwitchConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get clients_app's ID from user table
        $id_clients = \Auth::user()->fk_clients_app ;
        reconnect($id_clients);

        return $next($request);
    }
}
