<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\RolePermissionException ;
use Illuminate\Support\Facades\Auth;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions)
    {
        $user = Auth::user();
        // String to array
        $p_array = explode('|', $permissions);
        if ($user->havePermission($p_array))
            return $next($request);

        throw RolePermissionException::permissionException($p_array);
    }
}
