<?php

namespace App\Http\Requests\Propriete;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;


class ProprieteFormRequest extends FormRequest
{
    use SanitizesInput;


    /**
     * Call sanitize() methode
     * For appllying filters
     * Waavi\Sanitizer\Laravel\SanitizesInput::sanitize()
     *
     * @return void
     */
    public function validateResolved()
    {
        $this->sanitize();
        parent::validateResolved();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fk_copropriete' => ['required','exists:copropriete,id'],
            'num_propriete'  => ['digits']
        ];
    }

    /**
     * Validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fk_copropriete.required'   => 'La copropriété est obligatoire.',
            'fk_copropriete.exists'     => 'La copropriété est incorrecte.',
            'num_propriete.digits'      => 'num_propriete doit étre entier',
        ];
    }

    /**
     * Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'batiment' => 'trim|capitalize|escape'
        ];
    }
    
}
