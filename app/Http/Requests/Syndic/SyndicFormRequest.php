<?php

namespace App\Http\Requests\Syndic;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class SyndicFormRequest extends FormRequest
{
    use SanitizesInput;


    /**
     * Call sanitize() methode
     * For appllying filters
     * Waavi\Sanitizer\Laravel\SanitizesInput::sanitize()
     *
     * @return void
     */
    public function validateResolved()
    {
        $this->sanitize();
        parent::validateResolved();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fk_copropriete' => ['required','exists:copropriete,id']
        ];
    }

    /**
     * Validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'nom_syndic' => 'trim|capitalize|escape'
        ];
    }

}
