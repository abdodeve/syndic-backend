<?php

namespace App\Http\Controllers\ProprieteProprietaire;

use App\Models\ProprietaireModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProprieteProprietaireModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ProprieteProprietaire\ProprieteProprietaireFormRequest;


class ProprieteProprietaireController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste ProprieteProprietaire
     *
     * @param $request (id_copropriete)
     *
     * @return array $ProprieteProprietaire
     *
     */
    public function fetch(Request $request){

        $ProprieteProprietaires = DB::table('propriete_proprietaire')
                                    ->join('copropriete',
                                        'copropriete.id',
                                        '=',
                                        'propriete_proprietaire.fk_copropriete'
                                    )
                                    ->select('propriete_proprietaire.*')
                                    ->where([['copropriete.id', '=', $request->id_copropriete],
                                        ['propriete_proprietaire.status', '=', 'active']
                                    ])
                                    ->orderBy('propriete_proprietaire.id', 'desc')
                                    ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des propriete_proprietaire','propriete_proprietaire') ;

        return response()->json([   'success' => true, 
                                    'ProprieteProprietaires'=> $ProprieteProprietaires
                                ]);
    }

    /**
     * Single ProprieteProprietaire
     *
     * @param int $id
     *
     * @return array $ProprieteProprietaire, $previous, $next
     *
     */
    public function single(Request $request, $id) {
        $ProprieteProprietaire = ProprieteProprietaireModel::find($id) ;
        // If id not found
        if(!$ProprieteProprietaire) return response()->json(['error'=>'not_found']);

        // get previous ProprieteProprietaire id
        $previous = ProprieteProprietaireModel::where('id', '<', $ProprieteProprietaire->id)
            ->where('fk_copropriete', $ProprieteProprietaire->fk_copropriete)
            ->max('id');

        // get next ProprieteProprietaire id
        $next = ProprieteProprietaireModel::where('id', '>', $ProprieteProprietaire->id)
            ->where('fk_copropriete', $ProprieteProprietaire->fk_copropriete)
            ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un ProprieteProprietaire - id: {$id}",'ProprieteProprietaire') ;

        return response()->json([   'success' => true, 
                                    'ProprieteProprietaire'=> $ProprieteProprietaire,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert ProprieteProprietaire
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(ProprieteProprietaireFormRequest $request){

        /*******************************************
         * Insert new ProprieteProprietaire
         *******************************************/
        $ProprieteProprietaire = new ProprieteProprietaireModel();

        $ProprieteProprietaire->fk_copropriete   = $request->fk_copropriete ;
        $ProprieteProprietaire->fk_propriete     = $request->fk_propriete ;
        $ProprieteProprietaire->fk_proprietaire  = $request->fk_proprietaire ;
        $ProprieteProprietaire->status           = 'active' ;

        $ProprieteProprietaire->save();

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['ProprieteProprietaire' => ['insertedObject' => $ProprieteProprietaire]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout ProprieteProprietaire - id: {$ProprieteProprietaire->id}",'ProprieteProprietaire') ;

        return response()->json([   'success' => true, 
                                    'inserted_ProprieteProprietaire' => $ProprieteProprietaire
                                ]);
    }

    /**
     * Update ProprieteProprietaire
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $ProprieteProprietaire
     *
     */
    public function update(ProprieteProprietaireFormRequest $request, $id){
        $ProprieteProprietaire = ProprieteProprietaireModel::find($id);

        // If propriete_proprietaire ID not found
        if(!$ProprieteProprietaire) return response()->json(['error' => 'not_found', 'message' => 'ProprieteProprietaire ID not found - id: '.$id]);

        $ProprieteProprietaire->fk_copropriete   = $request->fk_copropriete ;
        $ProprieteProprietaire->fk_propriete     = $request->fk_propriete ;
        $ProprieteProprietaire->fk_proprietaire  = $request->fk_proprietaire ;

        $ProprieteProprietaire->save();

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['ProprieteProprietaire' => ['updatedObject' => $ProprieteProprietaire]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification ProprieteProprietaire - id: {$ProprieteProprietaire->id}",'ProprieteProprietaire') ;

        return response()->json([   'success' => true, 
                                    'updated_ProprieteProprietaire' => $ProprieteProprietaire
                                ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted ProprieteProprietaire
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $ProprieteProprietaire
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $propriete_proprietaires = DB::table('propriete_proprietaire')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'propriete_proprietaire.fk_copropriete'
            )
            ->select('propriete_proprietaire.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['propriete_proprietaire.status', '=', 'deleted']
            ])
            ->orderBy('propriete_proprietaire.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les propriete_proprietaires en corbeille",'propriete_proprietaire') ;

        return response()->json([   'success' => true, 
                                    'fetch_deleted_ProprieteProprietaire' => $propriete_proprietaires
                                ]);
    }

    /**
     * Restore ProprieteProprietaire
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $propriete_proprietaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('propriete_proprietaire')
            ->whereIn ('id', $propriete_proprietaires_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $propriete_proprietaires = ProprieteProprietaireModel::find($propriete_proprietaires_ids) ;
        // If propriete_proprietaires ID not found
        if($propriete_proprietaires->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $propriete_proprietaires_ids_str = implode(', ',$propriete_proprietaires_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['propriete_proprietaire' => ['restored_id' => $propriete_proprietaires_ids,
            'copropriete_id' => $propriete_proprietaires[0]->fk_copropriete,
            'restoredObjects' => $propriete_proprietaires]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration propriete_proprietaire - id: [ {$propriete_proprietaires_ids_str} }",'propriete_proprietaire') ;

        return response()->json([   'success' => true, 
                                    'restored_id' => $propriete_proprietaires_ids
                                ]);
    }


    /**
     * Delete Permanently ProprieteProprietaire(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $ProprieteProprietaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        $ProprieteProprietaire        = ProprieteProprietaireModel::find($ProprieteProprietaires_ids[0]) ;

        // If ProprieteProprietaire ID not found
        if(!$ProprieteProprietaire) return response()->json(['error' => 'not_found', 'message' => 'ProprieteProprietaire ID not found - id: '.$ProprieteProprietaires_ids[0]]);

        ProprieteProprietaireModel::destroy($ProprieteProprietaires_ids);

        // Array to string
        $ProprieteProprietaires_ids_str = implode(', ', $ProprieteProprietaires_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['ProprieteProprietaire' => ['deletedPermanently_id' => $ProprieteProprietaires_ids,
            'copropriete_id' => $ProprieteProprietaire->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement ProprieteProprietaire - id: { {$ProprieteProprietaires_ids_str} }",'ProprieteProprietaire') ;

        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $ProprieteProprietaires_ids
                                ]);
    }


    /**
     * Get proprietes of a proprietaire
     *
     * @param mixed $request (proprietaire_id)
     *
     * @return mixed $proprietes
     *
     */
    public function getProprietes(Request $request){

        // Validate Inputs
        $request->validate([
            'proprietaire_ids'   => 'required',
        ]);

        // Check if array or int
        $proprietaire_ids   = is_array($request->proprietaire_ids) ? $request->proprietaire_ids : [$request->proprietaire_ids] ;

        // Retrieve
        $proprietes =  \DB::table('propriete_proprietaire')
                        ->join('propriete', 'propriete.id', '=', 'propriete_proprietaire.fk_propriete')
                        ->whereIn ('propriete_proprietaire.fk_proprietaire', $proprietaire_ids)
                        ->where('propriete_proprietaire.status', 'active')
                        ->get(['propriete.id as id',
                               'propriete_proprietaire.id as propriete_proprietaire_id',
                               'propriete.num_propriete',
                               'propriete.type_copropriete',
                               'propriete.etage',
                               'propriete.num_titre',
                               'propriete.surface',
                               ]);

        return response()->json(['success' => true,
            'proprietes' => $proprietes
        ]);
    }



    /**
     * Get proprietaires of a propriete
     *
     * @param mixed $request (propriete_id)
     *
     * @return mixed $proprietaires
     *
     */
    public function getProprietaires(Request $request){

        // Validate Inputs
        $request->validate([
            'propriete_ids'   => 'required',
        ]);

        // Check if array or int
        $propriete_ids   = is_array($request->propriete_ids) ? $request->propriete_ids : [$request->propriete_ids] ;

        // Retrieve
        $proprietaires =  \DB::table('propriete_proprietaire')
            ->join('proprietaire', 'proprietaire.id', '=', 'propriete_proprietaire.fk_proprietaire')
            ->whereIn ('propriete_proprietaire.fk_propriete', $propriete_ids)
            ->where('propriete_proprietaire.status', 'active')
            ->get(['proprietaire.*']);

        return response()->json(['success' => true,
            'proprietes' => $proprietaires
        ]);
    }



    /**
     * assignProprietesToProprietaire
     * Assign Proprietes to a proprietaire
     *
     * @param mixed $request (id_copropriete, propriete_id, proprietaire_id)
     *
     * @return mixed $proprietes - Assigned proprietes
     *
     */
    public function assignProprietesToProprietaire(Request $request){
        $propriete_proprietaire = [] ;

        // Validate Inputs
        $request->validate([
            'id_copropriete' => 'required',
            'propriete_ids'   => 'required',
            'proprietaire_id' => 'required',
        ]);

        // Check if array or int
        $propriete_ids   = is_array($request->propriete_ids) ? $request->propriete_ids : [$request->propriete_ids] ;

        // Check if not Duplicate & Change status to active
        foreach ($propriete_ids as $propriete_id){
            $values = ['propriete_id' => $propriete_id,
                       'proprietaire_id' => $request->proprietaire_id] ;

            $isDuplicate = ProprieteProprietaireModel::isDuplicate($values);

            // If is duplicate continue
            if($isDuplicate) continue ;

            $propriete_proprietaire[] = ['fk_copropriete' => $request->id_copropriete,
                                         'fk_propriete' => $values['propriete_id'],
                                         'fk_proprietaire' => $values['proprietaire_id'],
                                         'status'=> 'active'] ;
        }

        $isDataSync = ProprieteProprietaireModel::insert($propriete_proprietaire);

        return response()->json(['success' => true,
            'isDataSync' => $isDataSync
        ]);
    }


    /**
     * Assign Proprietes to a proprietaire
     *
     * @param mixed $request (id_copropriete, propriete_id, proprietaire_id)
     *
     * @return mixed $proprietes - Assigned proprietes
     *
     */
    public function assignProprietairesToPropriete(Request $request){
        $propriete_proprietaire = [] ;

        // Validate Inputs
        $request->validate([
            'id_copropriete'    => 'required',
            'proprietaire_ids'  => 'required',
            'propriete_id'      => 'required',
        ]);

        // Check if array or int
        $proprietaire_ids   = is_array($request->proprietaire_ids) ? $request->proprietaire_ids : [$request->proprietaire_ids] ;

        // Check if not Duplicate & Change status to active
        foreach ($proprietaire_ids as $proprietaire_id){
            $values = ['propriete_id' => $request->propriete_id,
                       'proprietaire_id' => $proprietaire_id,
                      ];
            $isDuplicate = ProprieteProprietaireModel::isDuplicate($values);

            // If is duplicate continue
            if($isDuplicate) continue ;

            $propriete_proprietaire[] = ['fk_copropriete'   => $request->id_copropriete,
                                         'fk_propriete'     => $values['propriete_id'],
                                         'fk_proprietaire'  => $values['proprietaire_id'],
                                         'status'=> 'active'] ;
        }

        $isDataSync = ProprieteProprietaireModel::insert($propriete_proprietaire);

        return response()->json(['success' => true,
            'isDataSync' => $isDataSync
        ]);
    }



}
