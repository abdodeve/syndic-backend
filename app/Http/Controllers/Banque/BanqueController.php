<?php

namespace App\Http\Controllers\Banque;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BanqueModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Banque\BanqueFormRequest;

class BanqueController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste Banques
     *
     * @param $request (id_copropriete)
     *
     * @return array $banques
     *
     */
    public function fetch(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete' => 'required',
        ]);

        $banques = DB::table('banque')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'banque.fk_copropriete'
            )
            ->select('banque.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['banque.status', '=', 'active']
            ])
            ->orderBy('banque.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des banques','banque') ;

        return response()->json(['success' => true, 'banques'=> $banques]);
    }

    /**
     * Single Banque
     *
     * @param int $id
     *
     * @return array $banque, $previous, $next
     *
     */
    public function single(Request $request,$id) {

        $banque = BanqueModel::find($id) ;
        // If id not found
        if(!$banque) return response()->json(['error'=>'not_found']);

        // get previous banque id
        $previous = BanqueModel::where('id', '<', $banque->id)
            ->where('fk_copropriete', $banque->fk_copropriete)
            ->max('id');

        // get next banque id
        $next = BanqueModel::where('id', '>', $banque->id)
            ->where('fk_copropriete', $banque->fk_copropriete)
            ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un banque - id: {$id}",'banque') ;

        return response()->json([   'success' => true, 
                                    'banque'=> $banque,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert Banque
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(BanqueFormRequest $request){

        /*******************************************
         * Insert new banque
         *******************************************/
        $banque = new BanqueModel();

        $banque->fk_copropriete   = $request->fk_copropriete ;
        $banque->nom_banque       = $request->nom_banque ;
        $banque->cle_rib          = $request->cle_rib ;
        $banque->titulaire        = $request->titulaire ;
        $banque->code_banque      = $request->code_banque ;
        $banque->domiciliation    = $request->domiciliation ;
        $banque->code_postale     = $request->code_postale ;
        $banque->status           = 'active' ;

        $banque->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['banque' => ['insertedObject' => $banque]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout banque - id: {$banque->id}",'banque') ;

        return response()->json(['success' => true, 'inserted_banque' => $banque]);
    }

    /**
     * Update Banque
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $banque
     *
     */
    public function update(BanqueFormRequest $request, $id){


        $banque = BanqueModel::find($id);

        // If proprietaire ID not found
        if(!$banque) return response()->json(['error' => 'not_found', 'message' => 'Banque ID not found - id: '.$id]);

        $banque->fk_copropriete   = $request->fk_copropriete ;
        $banque->nom_banque       = $request->nom_banque ;
        $banque->cle_rib          = $request->cle_rib ;
        $banque->titulaire        = $request->titulaire ;
        $banque->code_banque      = $request->code_banque ;
        $banque->domiciliation    = $request->domiciliation ;
        $banque->code_postale     = $request->code_postale ;

        $banque->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['banque' => ['updatedObject' => $banque]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification banque - id: {$banque->id}",'banque') ;

        return response()->json(['success' => true, 'inserted_banque' => $banque]);
    }

    /**
     * Delete Banque
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        $banques_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to delete
        \DB::table('banque')
            ->whereIn ('id', $banques_ids)
            ->update(['status'=> 'deleted']);

        // Retrieve fk_copropriete
        $banque = BanqueModel::find($banques_ids[0]) ;

        // If Banque ID not found
        if(!$banque) return response()->json(['error' => 'not_found', 'message' => 'Banque ID not found - id: '.$banques_ids[0]]);

        // Array to string
        $banques_ids_str = implode(', ',$banques_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['banque' => ['deleted_id' => $request->id, 'copropriete_id' => $banque->fk_copropriete]]))) ;

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression banque - id: { {$banques_ids_str} }",'banque') ;

        return response()->json(['success' => true, 'deleted_id' => $banques_ids_str]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted Banques
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $banque
     *
     */
    public function fetchDeleted(Request $request){

        $banque = DB::table('banque')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'banque.fk_copropriete'
            )
            ->select('banque.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['banque.status', '=', 'deleted']
            ])
            ->orderBy('banque.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les banques en corbeille",'banque') ;

        return response()->json(['success' => true, 'fetch_deleted_banques' => $banque]);
    }

    /**
     * Restore Banque
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

            $banques_ids = is_array($request->id) ? $request->id : [$request->id];

            // Change status to active
            \DB::table('banque')
                ->whereIn ('id', $banques_ids)
                ->update(['status'=> 'active']);

            // Retrieve fk_copropriete
            $banques = BanqueModel::find($banques_ids);
            // If banque ID not found
            if($banques->isEmpty()) return response()->json(['error' => 'not_found']);

            // Array to string
            $banques_ids_str = implode(', ', $banques_ids);

            /*******************************************
             * Send Event
             *******************************************/
            event(new EventGlobale(response()->json(['banque' => ['restored_id' => $request->id,
                                                                  'copropriete_id' => $banques[0]->fk_copropriete,
                                                                  'restoredObjects' => $banques,
                                                                  ]
            ])));

            /*******************************************
             * Save Activity_Log
             *******************************************/
            the_activity("Restauration banque - id: [ {$banques_ids_str} }", 'banque');

        return response()->json(['success' => true, 'restored_id' => $banques_ids]);
    }


    /**
     * Delete Permanently Banque(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Int Or Array
        $banques_ids = is_array($request->id) ? $request->id : [$request->id] ;

        $banque = BanqueModel::find($banques_ids[0]) ;

        // If Banque ID not found
        if(!$banque) return response()->json(['error' => 'not_found', 'message' => 'Banque ID not found - id: '.$banques_ids[0]]);

        BanqueModel::destroy($banques_ids) ;

        // Array to string
        $banques_ids_str = implode(', ', $banques_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['banque' => ['deletedPermanently_id' => $request->id,
            'copropriete_id' => $banque->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement banque - id: { {$banques_ids_str} }",'banque') ;

        return response()->json(['success' => true, 'deletedPermanently_id' => $banques_ids]);
    }

}
