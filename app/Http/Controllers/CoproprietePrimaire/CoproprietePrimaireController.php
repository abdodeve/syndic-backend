<?php

namespace App\Http\Controllers\CoproprietePrimaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CoproprietePrimaireModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CoproprietePrimaire\CoproprietePrimaireFormRequest;

class CoproprietePrimaireController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste CoproprietePrimaires
     *
     * @param $request (id_copropriete)
     *
     * @return array $copropriete_primaires
     *
     */
    public function fetch(Request $request){

        $copropriete_primaires = DB::table('copropriete_primaire')
            ->select('copropriete_primaire.*')
            ->where([['copropriete_primaire.status', '=', 'active']])
            ->orderBy('copropriete_primaire.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des copropriete_primaires','copropriete_primaire') ;

        return response()->json(['success' => true, 
                                 'copropriete_primaires'=> $copropriete_primaires
                               ]);
    }

    /**
     * Single CoproprietePrimaire
     *
     * @param int $id
     *
     * @return array $copropriete_primaire, $previous, $next
     *
     */
    public function single(Request $request, $id) {
        $copropriete_primaire = CoproprietePrimaireModel::find($id) ;
        // If id not found
        if(!$copropriete_primaire) return response()->json(['error'=>'not_found']);

        // get previous copropriete_primaire id
        $previous = CoproprietePrimaireModel::where('id', '<', $copropriete_primaire->id)
            ->max('id');

        // get next copropriete_primaire id
        $next = CoproprietePrimaireModel::where('id', '>', $copropriete_primaire->id)
            ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un copropriete_primaire - id: {$id}",'copropriete_primaire') ;

        return response()->json([   'success' => true,
                                    'copropriete_primaire'=> $copropriete_primaire,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert CoproprietePrimaire
     *
     * @param mixed $request (nom, prenom, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(CoproprietePrimaireFormRequest $request){

        /*******************************************
         * Insert new copropriete_primaire
         *******************************************/
        $copropriete_primaire = new CoproprietePrimaireModel();
        $copropriete_primaire->fk_type_copropriete              = $request->fk_type_copropriete ;
        $copropriete_primaire->nom                              = $request->nom ;
        $copropriete_primaire->slug                             = $request->slug ;
        $copropriete_primaire->ville                            = $request->ville ;
        $copropriete_primaire->adresse                          = $request->adresse ;
        $copropriete_primaire->description                      = $request->description ;
        $copropriete_primaire->code_postale                     = $request->code_postale ;
        $copropriete_primaire->pays                             = $request->pays ;
        $copropriete_primaire->date_reprise                     = $request->date_reprise ;
        $copropriete_primaire->plan                             = $request->plan ;
        $copropriete_primaire->designation_plan                 = $request->designation_plan ;
        $copropriete_primaire->titre_foncier	                = $request->titre_foncier	 ;
        $copropriete_primaire->surface_totale                   = $request->surface_totale ;
        $copropriete_primaire->totale_tantieme                  = $request->totale_tantieme ;
        $copropriete_primaire->penalite_taux                    = $request->penalite_taux ;
        $copropriete_primaire->is_penalite_exist                = $request->is_penalite_exist ;
        $copropriete_primaire->status                           = 'active' ;

        $copropriete_primaire->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete_primaire' => ['insertedObject' => $copropriete_primaire]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout copropriete_primaire - id: {$copropriete_primaire->id}",'copropriete_primaire') ;

        return response()->json([   'success' => true, 
                                    'inserted_copropriete_primaire' => $copropriete_primaire
                               ]);
    }

    /**
     * Update CoproprietePrimaire
     *
     * @param mixed $request (nom, prenom, status)
     * @param int $id
     *
     * @return object $copropriete_primaire
     *
     */
    public function update(CoproprietePrimaireFormRequest $request, $id){
        $copropriete_primaire = CoproprietePrimaireModel::findOrFail($id);
        $copropriete_primaire->fk_type_copropriete              = $request->fk_type_copropriete ;
        $copropriete_primaire->nom                              = $request->nom ;
        $copropriete_primaire->slug                             = $request->slug ;
        $copropriete_primaire->ville                            = $request->ville ;
        $copropriete_primaire->adresse                          = $request->adresse ;
        $copropriete_primaire->description                      = $request->description ;
        $copropriete_primaire->code_postale                     = $request->code_postale ;
        $copropriete_primaire->pays                             = $request->pays ;
        $copropriete_primaire->date_reprise                     = $request->date_reprise ;
        $copropriete_primaire->plan                             = $request->plan ;
        $copropriete_primaire->designation_plan                 = $request->designation_plan ;
        $copropriete_primaire->titre_foncier	                = $request->titre_foncier	 ;
        $copropriete_primaire->surface_totale                   = $request->surface_totale ;
        $copropriete_primaire->totale_tantieme                  = $request->totale_tantieme ;
        $copropriete_primaire->penalite_taux                    = $request->penalite_taux ;
        $copropriete_primaire->is_penalite_exist                = $request->is_penalite_exist ;
        $copropriete_primaire->status                           = 'active' ;

        $copropriete_primaire->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete_primaire' => ['updatedObject' => $copropriete_primaire]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification copropriete_primaire - id: {$copropriete_primaire->id}",'copropriete_primaire') ;

        return response()->json([   'success' => true, 
                                    'updated_copropriete_primaire' => $copropriete_primaire
                               ]);
    }

    /**
     * Delete CoproprietePrimaire
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $copropriete_primaires_ids = is_array($request->id) ? $request->id : [$request->id] ;


        // Change status to delete
        \DB::table('copropriete')
            ->whereIn ('id', $copropriete_primaires_ids)
            ->update(['status'=> 'deleted']);

        // Retrieve fk_copropriete
        $copropriete_primaire = CoproprietePrimaireModel::find($copropriete_primaires_ids[0]) ;

        // If Proprietaire ID not found
        if(!$copropriete_primaire) return response()->json(['error' => 'not_found', 'message' => 'Copropriete_primaire ID not found - id: '.$copropriete_primaires_ids[0]]);

        // Array to string
        $copropriete_primaires_ids_str = implode(', ', $copropriete_primaires_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['copropriete_primaire' => ['deleted_id' => $copropriete_primaires_ids,
                                                                            'copropriete_id' => $copropriete_primaire->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression copropriete_primaire - id: { {$copropriete_primaires_ids_str} }",'copropriete_primaire') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $copropriete_primaires_ids
                                ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted CoproprietePrimaires
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $copropriete_primaire
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $copropriete_primaire = DB::table('copropriete_primaire')
            ->select('copropriete_primaire.*')
            ->where([['copropriete_primaire.status', '=', 'deleted']])
            ->orderBy('copropriete_primaire.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les CoproprietePrimaires en corbeille",'copropriete_primaire') ;

        return response()->json(['success' => true, 
                                 'fetch_deleted_copropriete_primaires' => $copropriete_primaire
                                ]);
    }

    /**
     * Restore CoproprietePrimaire
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $copropriete_primaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('copropriete_primaire')
            ->whereIn ('id', $copropriete_primaires_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $copropriete_primaires = CoproprietePrimaireModel::find($copropriete_primaires_ids) ;
        // If copropriete_primaires ID not found
        if($copropriete_primaires->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $copropriete_primaires_ids_str = implode(', ',$copropriete_primaires_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete_primaire' => ['restored_id' => $request->id,
            'restoredObjects' => $copropriete_primaires]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration copropriete_primaire - id: [ {$copropriete_primaires_ids_str} }",'copropriete_primaire') ;

        return response()->json([   'success' => true, 
                                    'restored_id' => $copropriete_primaires_ids
                                ]);
    }


    /**
     * Delete Permanently CoproprietePrimaire(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $copropriete_primaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        $copropriete_primaire = CoproprietePrimaireModel::find($copropriete_primaires_ids[0]) ;
        // If Proprietaire ID not found
        if(!$copropriete_primaire) return response()->json(['error' => 'not_found', 'message' => 'Copropriete_primaire ID not found - id: '.$copropriete_primaires_ids[0]]);

        CoproprietePrimaireModel::destroy($copropriete_primaires_ids);

        // Array to string
        $copropriete_primaires_ids_str = implode(', ', $copropriete_primaires_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete_primaire' => ['deletedPermanently_id' => $request->id]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement copropriete_primaire - id: { {$copropriete_primaires_ids_str} }",'copropriete_primaire') ;

        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $copropriete_primaires_ids
                                ]);
    }
}
