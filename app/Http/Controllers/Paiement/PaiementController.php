<?php

namespace App\Http\Controllers\Paiement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaiementModel;
use App\Models\PieceJointeModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Paiement\PaiementFormRequest;
use Validator;


class PaiementController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Important !
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * # mode_paiement - Liste mode_paiement Possible
     * ------------------------------------------------
     *  + virement_bancaire
     *  + chéque
     *  + espéce
     *
     * ------------------------------------------------
     */


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste Paiements
     *
     * @param $request (id_copropriete)
     *
     * @return array $paiements
     *
     */
    public function fetch(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        // Retrieve Paiements
        $paiements = DB::table('paiement')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'paiement.fk_copropriete'
            )
            ->select('paiement.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['paiement.status', '=', 'active']
            ])
            ->orderBy('paiement.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des paiements','paiement') ;

        return response()->json(['success' => true, 'paiements'=> $paiements]);
    }

    /**
     * Single Paiement
     *
     * @param int $id
     *
     * @return array $paiement, $previous, $next
     *
     */
    public function single(Request $request, $id) {

        $paiement = PaiementModel::find($id) ;

        // If id not found
        if(!$paiement) return response()->json(['error'=>'not_found']);

        // get previous paiement id
        $previous = PaiementModel::where('id', '<', $paiement->id)
            ->where('fk_copropriete', $paiement->fk_copropriete)
            ->max('id');

        // get next paiement id
        $next = PaiementModel::where('id', '>', $paiement->id)
            ->where('fk_copropriete', $paiement->fk_copropriete)
            ->min('id');

        // Retrieve Pieces
        $piece_jointes = PieceJointeModel::fetch('paiement', $paiement->id);

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un paiement - id: {$id}",'paiement') ;

        return response()->json([
            'paiement'=> $paiement,
            'piece_jointes' => $piece_jointes,
            'previous'=> $previous,
            'next'=> $next
        ]);
    }

    /**
     * Insert Paiement
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(PaiementFormRequest $request){

        /*******************************************
         * Insert new paiement
         *******************************************/
        $paiement = new PaiementModel();
        $paiement->fk_copropriete           = $request->fk_copropriete ;
        $paiement->fk_facture               = $request->fk_facture ;
        $paiement->fk_banque                = $request->fk_banque ;
        $paiement->mode_paiement            = $request->mode_paiement ;
        $paiement->num_paiement             = $request->num_paiement ;
        $paiement->date_paiement            = $request->date_paiement ;
        $paiement->mode_paiement            = $request->mode_paiement ;
        $paiement->affectation_paiement     = $request->affectation_paiement ;
        $paiement->montant                  = $request->montant ;
        $paiement->n_piece                  = $request->n_piece ;
        $paiement->piece_jointe             = $request->piece_jointe ;
        $paiement->libelle                  = $request->libelle ;
        $paiement->status                   = 'active' ;

        $paiement->save();

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['paiement' => ['insertedObject' => $paiement]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout paiement - id: {$paiement->id}",'paiement') ;

        return response()->json(['success' => true, 'inserted_paiement' => $paiement]);
    }

    /**
     * Update Paiement
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $paiement
     *
     */
    public function update(PaiementFormRequest $request, $id){
        $paiement = PaiementModel::find($id);

        // If proprietaire ID not found
        if(!$paiement) return response()->json(['error' => 'not_found', 'message' => 'Paiement ID not found - id: '.$id]);

        $paiement->fk_copropriete           = $request->fk_copropriete ;
        $paiement->fk_facture               = $request->fk_facture ;
        $paiement->fk_banque                = $request->fk_banque ;
        $paiement->mode_paiement            = $request->mode_paiement ;
        $paiement->num_paiement             = $request->num_paiement ;
        $paiement->date_paiement            = $request->date_paiement ;
        $paiement->mode_paiement            = $request->mode_paiement ;
        $paiement->affectation_paiement     = $request->affectation_paiement ;
        $paiement->montant                  = $request->montant ;
        $paiement->n_piece                  = $request->n_piece ;
        $paiement->piece_jointe             = $request->piece_jointe ;
        $paiement->libelle                  = $request->libelle ;

        $paiement->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['paiement' => ['updatedObject' => $paiement]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification paiement - id: {$paiement->id}",'paiement') ;

        return response()->json(['success' => true, 'updated_paiement' => $paiement]);
    }

    /**
     * Delete Paiement
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $paiements_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to deleted
        \DB::table('paiement')
            ->whereIn ('id', $paiements_ids)
            ->update(['status'=> 'deleted']);

        /*******************************************
         * Delete Pieces Soft
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('paiement', $paiements_ids);
        $piece_jointes = PieceJointeModel::deleteSoft($getPiecesIds);

        // Retrieve fk_copropriete
        $paiement = PaiementModel::find($paiements_ids[0]) ;

        // Array to string
        $paiements_ids_str = implode(', ', $paiements_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['paiement' => ['deleted_id' => $paiements_ids,
            'copropriete_id' => $paiement->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression paiement - id: { {$paiements_ids_str} }",'paiement') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $request->id, 
                                    'deleted_pieces'=> $piece_jointes
                                ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted Paiements
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $paiement
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $paiements = DB::table('paiement')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'paiement.fk_copropriete'
            )
            ->select('paiement.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['paiement.status', '=', 'deleted']
            ])
            ->orderBy('paiement.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les paiements en corbeille",'paiement') ;

        return response()->json(['success' => true, 'fetch_deleted_paiements' => $paiements]);
    }

    /**
     * Restore Paiement
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        $paiements = null ;
        // Check if array or int
        $paiements_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to deleted
        \DB::table('paiement')
            ->whereIn ('id', $paiements_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $paiements = PaiementModel::find($paiements_ids) ;
        // If paiements ID not found
        if($paiements->isEmpty()) return response()->json(['error' => 'not_found']);

        /*******************************************
         * Restore Pieces
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('paiement', $paiements_ids);
        $piece_jointes = PieceJointeModel::restore($getPiecesIds);

        // Array to string
        $paiements_ids_str = implode(', ',$paiements_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['paiement' => ['restored_id' => $paiements_ids,
            'copropriete_id' => $paiements[0]->fk_copropriete,
            'restoredObjects' => $paiements]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration paiement - id: [ {$paiements_ids_str} }",'paiement') ;

        return response()->json([   'success' => true, 
                                    'restored_id' => $request->id
                                ]);
    }


    /**
     * Delete Permanently Paiement(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $paiements_ids = is_array($request->id) ? $request->id : [$request->id] ;

        $paiement = PaiementModel::find($paiements_ids[0]) ;
        // If Paiement ID not found
        if(!$paiement) return response()->json(['error' => 'not_found', 'message' => 'Paiement ID not found - id: '.$paiements_ids[0]]);

        /*******************************************
         * Delete Pieces Permanently
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('paiement', $paiements_ids);
        $piece_jointes = PieceJointeModel::deletePermanently($getPiecesIds);

        PaiementModel::destroy($paiements_ids);

        // Array to String
        $paiements_ids_str = implode(', ', $paiements_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['paiement' => ['deletedPermanently_id' => $request->id,
            'copropriete_id' => $paiement->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement paiement - id: { {$paiements_ids_str} }",'paiement') ;

        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $request->id, 
                                    'pieces'=> $piece_jointes
                                ]);
    }


}
