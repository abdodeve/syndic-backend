<?php

namespace App\Http\Controllers\Facture;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FactureModel;
use App\Models\PieceJointeModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Facture\FactureFormRequest;

class FactureController extends Controller
{

 /*
 |-------------------------------------------------------------------------------------------------------------
 |  Primary
 |-------------------------------------------------------------------------------------------------------------
 */

    /**
     * Liste Factures
     *
     * @param $request (id_copropriete)
     *
     * @return array $factures
     *
     */
    public function fetch(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        // Retrieve Factures
        $factures = DB::table('facture')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'facture.fk_copropriete'
            )
            ->select('facture.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['facture.status', '=', 'active']
            ])
            ->orderBy('facture.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des factures','facture') ;

        return response()->json([   'success' => true, 
                                    'factures'=> $factures
                                ]);
    }

    /**
     * Single Facture
     *
     * @param int $id
     *
     * @return array $facture, $previous, $next
     *
     */
    public function single(Request $request, $id) {
        $facture = FactureModel::find($id) ;
        // If id not found
        if(!$facture) return response()->json(['error'=>'not_found']);

        // get previous facture id
        $previous = FactureModel::where('id', '<', $facture->id)
            ->where('fk_copropriete', $facture->fk_copropriete)
            ->max('id');

        // get next facture id
        $next = FactureModel::where('id', '>', $facture->id)
            ->where('fk_copropriete', $facture->fk_copropriete)
            ->min('id');

        // Retrieve Pieces
        $piece_jointes = PieceJointeModel::fetch('facture', $facture->id);

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un facture - id: {$id}",'facture') ;

        return response()->json([   'success' => true, 
                                    'facture'=> $facture,
                                    'piece_jointes' => $piece_jointes,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert Facture
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(FactureFormRequest $request){

        /*******************************************
         * Insert new facture
         *******************************************/
        $facture = new FactureModel();
        $facture->fk_copropriete    = $request->fk_copropriete ;
        $facture->fk_propriete      = $request->fk_propriete ;
        $facture->num_facture       = $request->num_facture ;
        $facture->date_facture      = $request->date_facture ;
        $facture->prestation        = $request->prestation ;
        $facture->libelle           = $request->libelle ;
        $facture->montant_facture   = $request->montant_facture ;
        $facture->montant_penalite  = $request->montant_penalite ;
        $facture->status            = 'active' ;

        $facture->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['facture' => ['insertedObject' => $facture]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout facture - id: {$facture->id}",'facture') ;

        return response()->json([   'success' => true, 
                                    'inserted_facture' => $facture
                                ]);
    }

    /**
     * Update Facture
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $facture
     *
     */
    public function update(FactureFormRequest $request, $id){
        $facture = FactureModel::find($id);

        // If proprietaire ID not found
        if(!$facture) return response()->json(['error' => 'not_found', 'message' => 'Facture ID not found - id: '.$id]);

        $facture->fk_copropriete    = $request->fk_copropriete ;
        $facture->fk_propriete      = $request->fk_propriete ;
        $facture->num_facture       = $request->num_facture ;
        $facture->date_facture      = $request->date_facture ;
        $facture->prestation        = $request->prestation ;
        $facture->libelle           = $request->libelle ;
        $facture->montant_facture   = $request->montant_facture ;
        $facture->montant_penalite  = $request->montant_penalite ;

        $facture->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['facture' => ['updatedObject' => $facture]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification facture - id: {$facture->id}",'facture') ;

        return response()->json([   'success' => true ,
                                    'updated_facture' => $facture
                               ]);
    }

    /**
     * Delete Facture
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $factures_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to deleted
        \DB::table('facture')
           ->whereIn ('id', $factures_ids)
           ->update(['status'=> 'deleted']);

        /*******************************************
         * Delete Pieces Soft
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('facture', $factures_ids);
        $piece_jointes = PieceJointeModel::deleteSoft($getPiecesIds);

        // Retrieve fk_copropriete
        $facture = FactureModel::find($factures_ids[0]) ;

        // Array to string
        $factures_ids = implode(', ', $factures_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['facture' => ['deleted_id' => $factures_ids,
            'copropriete_id' => $facture->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression facture - id: { {$factures_ids} }",'facture') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $factures_ids, 
                                    'pieces'=> $piece_jointes
                               ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted Factures
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $facture
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $factures = DB::table('facture')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'facture.fk_copropriete'
            )
            ->select('facture.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['facture.status', '=', 'deleted']
            ])
            ->orderBy('facture.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les factures en corbeille",'facture') ;

        return response()->json(['success' => true, 'fetch_deleted_factures' => $factures]);
    }

    /**
     * Restore Facture
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $factures = null ;
        // Check if array or int
        $factures_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to deleted
        \DB::table('facture')
            ->whereIn ('id', $factures_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $factures = FactureModel::find($factures_ids) ;
        // If factures ID not found
        if($factures->isEmpty()) return response()->json(['error' => 'not_found']);

        /*******************************************
         * Restore Pieces
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('facture', $factures_ids);
        $piece_jointes = PieceJointeModel::restore($getPiecesIds);

        // Array to string
        $factures_ids = implode(', ',$factures_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['facture' => ['restored_id' => $factures_ids,
            'copropriete_id' => $factures[0]->fk_copropriete,
            'restoredObjects' => $factures]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration facture - id: [ {$factures_ids} }",'facture') ;

        return response()->json([   'success' => true, 
                                    'restored_id' => $factures_ids, 
                                    'pieces'=> $piece_jointes
                               ]);
    }


    /**
     * Delete Permanently Facture(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $factures_ids = is_array($request->id) ? $request->id : [$request->id] ;

        $facture = FactureModel::find($factures_ids[0]) ;
        // If Facture ID not found
        if(!$facture) return response()->json(['error' => 'not_found', 'message' => 'Facture ID not found - id: '.$factures_ids[0]]);

        /*******************************************
         * Delete Pieces Permanently
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('facture', $factures_ids);
        $piece_jointes = PieceJointeModel::deletePermanently($getPiecesIds);

        FactureModel::destroy($factures_ids);

        // Array to String
        $factures_ids_str = implode(', ', $factures_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['facture' => ['deletedPermanently_id' => $factures_ids,
            'copropriete_id' => $facture->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement facture - id: { {$factures_ids_str} }",'facture') ;

        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $factures_ids, 
                                    'pieces'=> $piece_jointes
                               ]);
    }


}
