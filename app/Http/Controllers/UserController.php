<?php

namespace App\Http\Controllers;

use App\Http\Controllers\RolePermesssion\RolePermissionController;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\RolePermissionModel ;
use App\Models\RoleModel ;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\HasApiTokens;
use App\Mail\ForgotPassword;
use App\Mail\SignUpEmailToUser;
use App\Mail\SignUpEmailToUs;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    use HasApiTokens ;

    /**
     * Fetch All users
     *
     * @return array $users
     *
     */
   public function fetch (Request $request){
    // Fetch All Users
    $users = User::fetch() ;

    return response()->json([   'success' => true, 
                                'users' => $users
                            ]) ;
   }

    /**
     * Delete permanently
     *
     * @param $request ($id)
     *
     * @return int $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $users_ids   = is_array($request->id) ? $request->id : [$request->id] ;
        $users = User::find($users_ids) ;
        
        User::destroy($users);
        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $users_ids
                                ]);
    }

    /**
     * Single User
     *
     * @param int $id
     *
     * @return array ($user, $previous, $next)
     *
     */
    public function single(Request $request, $id) {

        // Get the user
        $user = \DB::connection('main')->table('users')
                                      ->select(['users.id', 'users.name', 'users.email', 'users.fk_roles'])
                                      ->where('users.id', $id)
                                      ->get();
        $user = $user[0] ;

        // If id not found
        if(!$user) return response()->json(['error'=>'not_found']);

        // get previous proprietaire id
        $previous = \DB::connection('main')->table('users')->where('id', '<', $user->id)->max('id');

        // get next proprietaire id
        $next = \DB::connection('main')->table('users')->where('id', '>', $user->id)->min('id');

        return response()->json(['success' => true, 
                                 'user'=> $user,
                                 'previous'=> $previous,
                                 'next'=> $next
                                ]);
    }



    /**
     * Insert User
     *
     * @param mixed $request ($name, $email, $password, $fk_roles)
     *
     * @return object $user
     *
     */
    public function insert(Request $request){

        // Is this User Exist
        $isExist = User::where('email', '=', $request->email)->exists() ;
        if($isExist) return response()->json(['error'=>'user_exist']) ;

        $user = new User();

        if($request->password)
        $user->password        = bcrypt($request->password) ;
        $user->name            = $request->name ;
        $user->email           = $request->email ;
        $user->fk_roles        = $request->fk_roles ;
        $user->fk_clients_app  = \AppSettings::clients_app_id() ;
        $user->active          = $request->active ;

        $user->save() ;

        return response()->json(['success' => true, 
                                 'inserted_user' => $user
                                ]);
    }



    /**
     * Update User
     *
     * @param mixed $request ($name, $email, $password, $fk_roles)
     * @param int $id
     *
     * @return array ($user, isSuperAdmin(), $role_in_request)
     *
     */
    public function update(Request $request, $id){

        // Check if this User Email Exist
        $u = User::where('email', '=', $request->email) ;
        if($u->exists() && $u->get()->first()->id != $id) return response()->json(['error'=>'user_exist']);

        // Retrieve role name of this user
        $user_auth = \Auth::user();

        // Retrieve role name from $request->fk_roles
        $role_in_request = RoleModel::where('id', '=', $request->fk_roles)
                                    ->first(['name']);
        $role_in_request = $role_in_request->name ;

        // If Auth User not Super-Admin & He try to set super-admin role to a user
        if(!$user_auth->isSuperAdmin() && $role_in_request=='super-admin')
            return response()->json(['error'=>'not_super_admin',
                'message'=> 'You can not set super admin role to others because you are not super admin, Just super-admin can do']);

            $user = User::find($id);
            if($request->password)
            $user->password        = bcrypt($request->password) ;
            $user->name            = $request->name ;
            $user->email           = $request->email ;
            $user->fk_roles        = $request->fk_roles ;
            $user->active          = $request->active ;

            $user->save() ;


        return response()->json(['success'=>'user_updated', 
                                 'updated_user'=> $user,
                                 'isSuperAdmin' => $user_auth->isSuperAdmin(), 
                                 'role_in_request' => $role_in_request
                                ]);
    }


    /**
     * User Log In
     *
     * @return array access_token
     *
     */
    public function login (Request $request){

        // validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email',
            'password' => 'required|alphaNum'
        );

        $request->validate($rules);

        // create our user credentials data for the authentication
        $userdata = array(
            'email'     => $request->email,
            'password'  => $request->password
        );


        // attempt to do the login
        if (!Auth::attempt($userdata)) 
            return response()->json(['error'=> 'invalid_credentials','message'=> 'Mot de passe est incorrecte']) ;

        // Creating a token
        $user = User::where('email', $request->email)->first();

        // Check if User not Active
        if($user->active == false) return response()->json(['error'=> 'user_not_active']);

        // Generate token
        $token = $user->createToken('Token Syndic')->accessToken ;

        // Reconnect to Client Tenant
        reconnect($user->fk_clients_app);
        
        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Utilisateur: {$user->email} est connecté",'user') ;

        return response()->json(['success' => true, 
                                 'access_token'=> $token
                                ]) ;
    }


    /**
    * Retrieve Logged In User
    *
    * @return object $user
    *
    */
    public function userLoggedIn (Request $request){
        $user = Auth::user();
        return response()->json(['success' => true, 
                                 'user'=> $user
                                ]);
    }

    /**
     * LogOut Session
     *
     * @return string success
     *
     */
      public function userLogout(Request $request)
      {
        //      if(!Auth::check())
        //          return response()->json(array('Error'=>'Unauthenticated user'));

              $user = User::find($request->user_id);
              if ($user) {
                  Auth::logout(); // TEMP Under Test
                  DB::table('oauth_access_tokens')
                      ->where('user_id', $user->id)
                      ->delete();
              }

            return response()->json(array(['success' => true, 
                                           'message'=>'Log out success'
                                          ]));
      }

    /**
     * Change password
     *
     * @param mixed $request (currentPassword, newPassword, repeatPassword)
     *
     * @return string success msg
     *
     */
      public function changePassword(Request $request){

        $currentPassword  = $request->currentPassword ;
        $newPassword      = $request->newPassword ;
        $repeatPassword   = $request->repeatPassword ;
        $user = Auth::user();

        if(!Hash::check($currentPassword, $user->password))
        return response()->json(array('error'=>'incorrect_password', 'message' => 'password incorrecte')) ;

        if($newPassword !== $repeatPassword)
        return response()->json(array('error'=>'not_match', 'message' => 'new passwords not match')) ;

        $user->password = bcrypt($request->newPassword) ;
        $user->save() ;
        return response()->json(array('success'=>'true',
                                      'message' => 'Password changed')) ;
      }

    /**
     * Forgot Password
     *
     * @param mixed $request (email)
     *
     * @return string success msg
     *
     */
      public function forgotPassword(Request $request) {
          // create rules
          $rules = array(
              'email'    => 'required|email',
          );
          $request->validate($rules);

        $email = $request->email ;
        $user = User::where('email', $email)->first();

        if(!$user) return response()->json(['error'=> 'user_not_found']) ;

        $new_random_password = str_random(5);
        $user->password = bcrypt($new_random_password) ;
        $user->save();

        // Create passedData Object for ForgotPassword
        $passedData = new \stdClass();
        $passedData->from = 'forgotPassword' ;
        $passedData->new_random_password = $new_random_password;
        // Send ForgotPassword
        Mail::to($user->email)->send(new ForgotPassword($passedData));

        return response()->json(['success' => true, 
                                 'message' => 'passowrd sent to email'
                                ]) ;
    }

    /**
     * signUp User
     *
     * @param mixed $request ($name, $email, $password, $fk_roles)
     *
     * @return object $user
     *
     */
    public function signUp(Request $request){

        // Validate - create rules
        $rules = array(
            'email'    => 'required|email',
            'password' => 'required|alphaNum'
        );
        $request->validate($rules);

        // Is this User Exist
        $isExist = User::where('email', '=', $request->email)->exists() ;
        if($isExist) return response()->json(['error'=>'user_exist']) ;

        // New User
        $user = new User();
        $user->email           = $request->email ;
        $user->password        = bcrypt($request->password) ;
        $user->name            = $request->name ;
        $user->fk_roles        = $request->fk_roles ;
        $user->active          = false ;
        $user->save() ;

        // Create passedData Object for ForgotPassword
        $passedData = new \stdClass();
        $passedData->from = 'signUp' ;
        $passedData->user = $user ;
        // Send Email to Us
        Mail::to('contact@marocgeek.com')->send(new SignUpEmailToUs($passedData));
        // Send Email to User
        Mail::to($user->email)->send(new SignUpEmailToUser($passedData));

        return response()->json(['success' => true, 'user' => $user]);
    }


    /**
     * Export excel of users
     *
     * @return void
     *
     */
    public function export(Request $request){
        return Excel::download(new UsersExport, 'users.xlsx');
    }
 
    /**
     * Import excel of users
     * @param mixed $request ($usersExcel)
     *
     * @return string message
     *
     */
    public function import(Request $request){

        $usersExcel  = $request->file('usersExcel');
        Excel::import(new UsersImport, $usersExcel);
        return response()->json(['success' => true, 'message'=> 'file imported successfully']);
    }

}