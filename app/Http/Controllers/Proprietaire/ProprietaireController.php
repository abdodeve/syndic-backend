<?php

namespace App\Http\Controllers\Proprietaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProprietaireModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Proprietaire\ProprietaireFormRequest;
use App\Exports\ProprietaireExport;
use App\Imports\ProprietaireImport;
use Maatwebsite\Excel\Facades\Excel;

class ProprietaireController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste Proprietaires
     *
     * @param $request (id_copropriete)
     *
     * @return array $proprietaires
     *
     */
    public function fetch(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete' => 'required',
        ]);

          $proprietaires = DB::table('proprietaire')
            ->join('copropriete',
                   'copropriete.id',
                    '=',
                    'proprietaire.fk_copropriete'
                    )
            ->select('proprietaire.*')
            ->where([['copropriete.id', '=', $request->id_copropriete], 
                    ['proprietaire.status', '=', 'active']
                    ])
            ->orderBy('proprietaire.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des proprietaires','proprietaire') ;

          return response()->json(['success' => true,
                                   'proprietaires'=> $proprietaires
                                  ]);
    }

    /**
     * Single Proprietaire
     *
     * @param int $id
     *
     * @return array $proprietaire, $previous, $next
     *
     */
    public function single(Request $request, $id) {

        $proprietaire = ProprietaireModel::find($id) ;
         // If id not found
         if(!$proprietaire) return response()->json(['error'=>'not_found', 'message'=> 'proprietaire not found']);

        // get previous proprietaire id
        $previous = ProprietaireModel::where('id', '<', $proprietaire->id)
                                     ->where('fk_copropriete', $proprietaire->fk_copropriete)
                                     ->max('id');

        // get next proprietaire id
        $next = ProprietaireModel::where('id', '>', $proprietaire->id)
                                 ->where('fk_copropriete', $proprietaire->fk_copropriete)
                                 ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un proprietaire - id: {$id}",'proprietaire') ;

        return response()->json(['success' => true,
                                 'proprietaire'=> $proprietaire,
                                 'previous'=> $previous,
                                 'next'=> $next
                                ]);
    }

    /**
     * Insert Proprietaire
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
     public function insert(ProprietaireFormRequest $request){

       /*******************************************
        * Insert new proprietaire
        *******************************************/
        $proprietaire = new ProprietaireModel();
        $proprietaire->fk_copropriete   = $request->fk_copropriete ;
        $proprietaire->nom              = $request->nom ;
        $proprietaire->prenom           = $request->prenom ;
        $proprietaire->cin              = $request->cin ;
        $proprietaire->profession       = $request->profession ;
        $proprietaire->titre            = $request->titre ;
        $proprietaire->ville            = $request->ville ;
        $proprietaire->adresse_1        = $request->adresse_1 ;
        $proprietaire->adresse_2        = $request->adresse_2 ;
        $proprietaire->email_1          = $request->email_1 ;
        $proprietaire->email_2          = $request->email_2 ;
        $proprietaire->tel_1            = $request->tel_1 ;
        $proprietaire->tel_2            = $request->tel_2 ;
        $proprietaire->code_postale     = $request->code_postale ;
        $proprietaire->description      = $request->description ;
        $proprietaire->status           = 'active' ;

        $proprietaire->save();


       /*******************************************
        * Send Event
        *******************************************/
        event(new EventGlobale(response()->json(['success' => true,
                                                 'proprietaire' => ['insertedObject' => $proprietaire]
                                                ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout proprietaire - id: {$proprietaire->id}",'proprietaire') ;

        return response()->json(['success' => true, 'inserted_proprietaire' => $proprietaire]);
    }

    /**
     * Update Proprietaire
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $proprietaire
     *
     */
    public function update(ProprietaireFormRequest $request, $id){

        $proprietaire = ProprietaireModel::find($id);

        // If proprietaire ID not found
         if(!$proprietaire) return response()->json(['error' => 'not_found', 'message' => 'Proprietaire ID not found - id: '.$id]);

        $proprietaire->fk_copropriete   = $request->fk_copropriete ;
        $proprietaire->nom              = $request->nom ;
        $proprietaire->prenom           = $request->prenom ;
        $proprietaire->cin              = $request->cin ;
        $proprietaire->profession       = $request->profession ;
        $proprietaire->titre            = $request->titre ;
        $proprietaire->ville            = $request->ville ;
        $proprietaire->adresse_1        = $request->adresse_1 ;
        $proprietaire->adresse_2        = $request->adresse_2 ;
        $proprietaire->email_1          = $request->email_1 ;
        $proprietaire->email_2          = $request->email_2 ;
        $proprietaire->tel_1            = $request->tel_1 ;
        $proprietaire->tel_2            = $request->tel_2 ;
        $proprietaire->code_postale     = $request->code_postale ;
        $proprietaire->description      = $request->description ;

        $proprietaire->save();


       /*******************************************
        * Send Event
        *******************************************/
        event(new EventGlobale(response()->json(['proprietaire' => ['updatedObject' => $proprietaire]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification proprietaire - id: {$proprietaire->id}",'proprietaire') ;

      	return response()->json(['success' => true, 'updated_proprietaire' => $proprietaire]);
    }

    /**
     * Delete Proprietaire
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
     public function delete(Request $request){

         // Validate Inputs
         $request->validate([
             'id'   => 'required',
         ]);

         // Check if array or int
        $proprietaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to delete
        \DB::table('proprietaire')
           ->whereIn ('id', $proprietaires_ids)
           ->update(['status'=> 'deleted']);

        // Retrieve fk_copropriete
        $proprietaire = ProprietaireModel::find($proprietaires_ids[0]) ;

         // If Proprietaire ID not found
         if(!$proprietaire) return response()->json(['error' => 'not_found', 'message' => 'Proprietaire ID not found - id: '.$proprietaires_ids[0]]);

         // Array to string
         $proprietaires_ids_str = implode(', ',$proprietaires_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['proprietaire' => ['deleted_id' => $proprietaires_ids,
                                                 'copropriete_id' => $proprietaire->fk_copropriete,]
        ])));

         /*******************************************
          * Save Activity_Log
          *******************************************/
         the_activity("Suppression proprietaire - id: { {$proprietaires_ids_str} }",'proprietaire') ;

        return response()->json(['success' => true, 'deleted_id' => $proprietaires_ids]);
    }

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted Proprietaires
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $proprietaire
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $proprietaires = DB::table('proprietaire')
            ->join('copropriete',
                    'copropriete.id',
                    '=',
                    'proprietaire.fk_copropriete'
                    )
            ->select('proprietaire.*')
            ->where([['copropriete.id', '=', $request->id_copropriete], 
                    ['proprietaire.status', '=', 'deleted']
                    ])
            ->orderBy('proprietaire.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les proprietaires en corbeille",'proprietaire') ;

        return response()->json(['success' => true, 'fetch_deleted_proprietaires' => $proprietaires]);
    }

    /**
     * Restore Proprietaire
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $proprietaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('proprietaire')
            ->whereIn ('id', $proprietaires_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $proprietaires = ProprietaireModel::find($proprietaires_ids) ;
        // If Proprietaire ID not found
        if($proprietaires->isEmpty()) return response()->json(['error' => 'not_found']);


        // Array to string
        $proprietaires_ids_str = implode(', ',$proprietaires_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['proprietaire' => ['restored_id' => $proprietaires_ids,
                                                                    'copropriete_id' => $proprietaires[0]->fk_copropriete,
                                                                    'restoredObjects' => $proprietaires]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration proprietaire - id: [ {$proprietaires_ids_str} }",'proprietaire') ;

        return response()->json(['success' => true, 'restored_id' => $proprietaires_ids]);
    }


    /**
     * Delete Permanently Proprietaire(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $proprietaires_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        $proprietaire        = ProprietaireModel::find($proprietaires_ids[0]) ;
        // If Proprietaire ID not found
        if(!$proprietaire) return response()->json(['error' => 'not_found', 'message' => 'Proprietaire ID not found - id: '.$proprietaires_ids[0]]);

        ProprietaireModel::destroy($proprietaires_ids);

        // Array to string
        $proprietaires_ids_str = implode(', ', $proprietaires_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['proprietaire' => ['deletedPermanently_id' => $proprietaires_ids,
                                                                    'copropriete_id' => $proprietaire->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement proprietaire - id: { {$proprietaires_ids_str} }",'proprietaire') ;

        return response()->json(['success' => true, 'deletedPermanently_id' => $proprietaires_ids]);
    }

    

    /**
     * Export excel of users
     *
     * @return void
     *
     */
    public function export(Request $request){
        
        return Excel::download(new ProprietaireExport, 'Proprietaires.xlsx');
    }
 
    /**
     * Import excel of users
     * @param mixed $request ($usersExcel)
     *
     * @return string message
     *
     */
    public function import(Request $request){

        $proprietaireExcel  = $request->file('proprietaireExcel');
        $proprietaireImport = new ProprietaireImport ;
        Excel::import($proprietaireImport, $proprietaireExcel);
        
        $copropriete_id = $proprietaireImport->importedObjects[0]->fk_copropriete ; // $request->header('copropriete_id');

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['proprietaire' => ['copropriete_id' => $copropriete_id,
                                                                    'importedObjects' => $proprietaireImport->importedObjects]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Importation des proprietaires",'proprietaire') ;
        
        return response()->json(['success' => true, 'message'=> 'file imported successfully', 'copropriete_id'=> $copropriete_id]);
    }

    /**
     * Generate pdf report
     *
     * @return string message
     *
     */
    public function generatePDF(Request $request){
        $data = ['title' => 'Welcome to Proprietaire'];
        $pdf = \PDF::loadView('pdf/myPDF', $data);
    
        // return $pdf->stream();
        return $pdf->download('proprietaire.pdf');
    }
      
}
