<?php

namespace App\Http\Controllers\Propriete;

use App\Events\EventGlobale;
use Illuminate\Http\Request;
use App\Models\ProprieteModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Propriete\ProprieteFormRequest;

class ProprieteController extends Controller
{
    /*
|-------------------------------------------------------------------------------------------------------------
|  Primary
|-------------------------------------------------------------------------------------------------------------
*/

    /**
     * Liste Proprietes
     *
     * @param $request (id_copropriete)
     *
     * @return array $proprietes
     *
     */
        public function fetch(Request $request){

            $proprietes = DB::table('propriete')
                            ->join('copropriete',
                                'copropriete.id',
                                '=',
                                'propriete.fk_copropriete'
                            )
                            ->select('propriete.*')
                            ->where([['copropriete.id', '=', $request->id_copropriete],
                                ['propriete.status', '=', 'active']
                            ])
                            ->orderBy('propriete.id', 'desc')
                            ->get();
            // $proprietes = DB::table('propriete')->get();
            /*******************************************
             * Save Activity_Log
             *******************************************/
            the_activity('Affichage liste des proprietes','propriete') ;

            return response()->json([   'success' => true, 
                                        'proprietes'=> $proprietes
                                    ]);
        }
    /**
     * Single Propriete
     *
     * @param int $id
     *
     * @return array $propriete, $previous, $next
     *
     */
        public function single(Request $request,$id) {
            $propriete = ProprieteModel::find($id) ;
            // If id not found
            if(!$propriete) return response()->json(['error'=>'not_found']);

            // get previous propriete id
            $previous = ProprieteModel::where('id', '<', $propriete->id)
                                      ->where('fk_copropriete', $propriete->fk_copropriete)
                                      ->max('id') ;

            // get next propriete id
            $next = ProprieteModel::where('id', '>', $propriete->id)
                                  ->where('fk_copropriete', $propriete->fk_copropriete)
                                  ->min('id') ;

            /*******************************************
            * Save Activity_Log
            *******************************************/
            the_activity("Affichage d\'un propriete - id: {$id}",'propriete') ;

            return response()->json([   'success' => true,
                                        'propriete'=> $propriete,
                                        'previous'=> $previous,
                                        'next'=> $next
                                    ]);
        }
    /**
     * Insert propriete
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $propriete
     *
     */
        public function insert(ProprieteFormRequest $request){
            
            $propriete = new ProprieteModel();
            $propriete->fk_copropriete          = $request->fk_copropriete ;
            $propriete->num_propriete           = $request->num_propriete ;
            $propriete->type_copropriete        = $request->type_copropriete ;
            $propriete->batiment                = $request->batiment ;
            $propriete->etage                   = $request->etage ;
            $propriete->num_titre               = $request->num_titre ;
            $propriete->surface                 = $request->surface ;
            $propriete->quote_par_terrain       = $request->quote_par_terrain ;
            $propriete->pt_indivision           = $request->pt_indivision ;
            $propriete->voix                    = $request->voix ;
            $propriete->taux_tantiem            = $request->taux_tantiem ;
            $propriete->commentaire             = $request->commentaire ;
            $propriete->type_utilisation        = $request->type_utilisation ;
            $propriete->n_tourne                = $request->n_tourne ;
            $propriete->police_eau              = $request->police_eau ;
            $propriete->article_impot           = $request->article_impot ;
            $propriete->status                  = 'active' ;
            $propriete->save();

            /*******************************************
            * Send Event
            *******************************************/
            event(new EventGlobale(response()->json(['propriete' => ['insertedObject' => $propriete]])));

            /*******************************************
             * Save Activity_Log
             *******************************************/
            the_activity("Ajout propriete - id: {$propriete->id}",'propriete') ;
            
            return response()->json([   'success' => true, 
                                        'inserted_propriete' => $propriete
                                    ]);

        }
     /**
     * Update Propriete
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $propriete
     *
     */
        public function update(Request $request, $id){

            $propriete = ProprieteModel::find($id);
            // If proprietaire ID not found
            if(!$propriete) return response()->json(['error' => 'not_found', 'message' => 'Propriete ID not found - id: '.$id]);

            $propriete->fk_copropriete          = $request->fk_copropriete ;
            $propriete->num_propriete           = $request->num_propriete ;
            $propriete->type_copropriete        = $request->type_copropriete ;
            $propriete->batiment                = $request->batiment ;
            $propriete->etage                   = $request->etage ;
            $propriete->num_titre               = $request->num_titre ;
            $propriete->surface                 = $request->surface ;
            $propriete->quote_par_terrain       = $request->quote_par_terrain ;
            $propriete->pt_indivision           = $request->pt_indivision ;
            $propriete->voix                    = $request->voix ;
            $propriete->taux_tantiem            = $request->taux_tantiem ;
            $propriete->commentaire             = $request->commentaire ;
            $propriete->type_utilisation        = $request->type_utilisation ;
            $propriete->n_tourne                = $request->n_tourne ;
            $propriete->police_eau              = $request->police_eau ;
            $propriete->article_impot           = $request->article_impot ;
            $propriete->status                  = $request->status ;
            $propriete->save();

            /*******************************************
            * Send Event
            *******************************************/
            event(new EventGlobale(response()->json(['Propriete' => ['updatedObject' => $propriete]])));

            /*******************************************
             * Save Activity_Log
             *******************************************/
            the_activity("Modifier Propriete - id: {$propriete->id}",'Propriete') ;

            return response()->json([   'success' => true,
                                        'updated_propriete' => $propriete
                                    ]);
        }


    /**
     * Delete Propriete
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
        public function delete(Request $request){
            // Validate Inputs
            $request->validate([
                'id'   => 'required',
            ]);

            // Check if array or int
            $proprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

            // Change status to delete
            \DB::table('propriete')
                ->whereIn ('id', $proprietes_ids)
                ->update(['status'=> 'deleted']);

            // Retrieve fk_copropriete
            $propriete = ProprieteModel::find($proprietes_ids[0]) ;

            // If Propriete ID not found
            if(!$propriete) return response()->json(['error' => 'not_found', 'message' => 'Propriete ID not found - id: '.$proprietes_ids[0]]);

            // Array to string
            $proprietes_ids_str = implode(', ',$proprietes_ids) ;

            /*******************************************
             * Send Event
             ******************************************/
            event(new EventGlobale(response()->json(['propriete' => ['deleted_id' => $proprietes_ids,
                'copropriete_id' => $propriete->fk_copropriete,]
            ])));

            /*******************************************
             * Save Activity_Log
             *******************************************/
            the_activity("Suppression propriete - id: { {$proprietes_ids_str} }",'propriete') ;

            return response()->json([   'success' => true, 
                                        'deleted_id' => $proprietes_ids
                                    ]);
        }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */
    /**
     * Fetch deleted proprietes
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $propriete
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $proprietes = DB::table('propriete')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'propriete.fk_copropriete'
            )
            ->select('propriete.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['propriete.status', '=', 'deleted']
            ])
            ->orderBy('propriete.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les proprietes en corbeille",'propriete') ;

        return response()->json(['success' => true, 'fetch_deleted_proprietes' => $proprietes]);
    }


    /**
     * Restore Propriete
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $proprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('propriete')
            ->whereIn ('id', $proprietes_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $proprietes = ProprieteModel::find($proprietes_ids) ;
        // If proprietes ID not found
        if($proprietes->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $proprietes_ids_str = implode(', ',$proprietes_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['propriete' => ['restored_id' => $proprietes_ids,
            'copropriete_id' => $proprietes[0]->fk_copropriete,
            'restoredObjects' => $proprietes]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration propriete - id: [ {$proprietes_ids_str} }",'propriete') ;

        return response()->json(['success' => true, 'restored_id' => $proprietes_ids]);
    }


    /**
     * Delete Permanently Propriete(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
        public function deletePermanently(Request $request){

            // Validate Inputs
            $request->validate([
                'id'   => 'required',
            ]);

            // Check if array or int
            $proprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

            $propriete = ProprieteModel::find($proprietes_ids[0]) ;

            // If Propriete ID not found
            if(!$propriete) return response()->json(['error' => 'not_found', 'message' => 'Propriete ID not found - id: '.$proprietes_ids[0]]);

            ProprieteModel::destroy($proprietes_ids);

            // Array to string
            $proprietes_ids_str = implode(', ', $proprietes_ids) ;

            /*******************************************
             * Send Event
             *******************************************/
            event(new EventGlobale(response()->json(['propriete' => ['deletedPermanently_id' => $proprietes_ids, 
                                                    'copropriete_id' => $propriete->fk_copropriete]
            ])));

            /*******************************************
             * Save Activity_Log
             *******************************************/
            the_activity("Suppression définitivement Propriete - id: { {$proprietes_ids_str} }",'propriete') ;

            return response()->json([   'success' => true, 
                                        'deletedPermanently_id' => $proprietes_ids
                                    ]);
        }
}