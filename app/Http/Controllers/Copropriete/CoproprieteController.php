<?php

namespace App\Http\Controllers\Copropriete;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CoproprieteModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Copropriete\CoproprieteFormRequest;
use App\Models\PieceJointeModel;

class CoproprieteController extends Controller
{

   /*
   |-------------------------------------------------------------------------------------------------------------
   |  Primary
   |-------------------------------------------------------------------------------------------------------------
   */

    /**
     * Liste coproprietes
     *
     * @param $request (id_copropriete)
     *
     * @return array $coproprietes
     *
     */
    public function fetch(Request $request){

        $coproprietes = DB::table('copropriete')
            ->select('copropriete.*')
            ->where([['copropriete.status', '=', 'active']])
            ->orderBy('copropriete.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des copropriete','copropriete') ;

        return response()->json(['success' => true, 
                                 'coproprietes'=> $coproprietes
                               ]);
    }

    /**
     * Single Copropriete
     *
     * @param int $id
     *
     * @return array $copropriete, $previous, $next
     *
     */
    public function single(Request $request,$id) {
        $copropriete = CoproprieteModel::find($id) ;
        // If id not found
        if(!$copropriete) return response()->json(['error'=>'not_found']);

        // get previous copropriete id
        $previous = CoproprieteModel::where('id', '<', $copropriete->id)
            ->max('id');

        // get next copropriete id
        $next = CoproprieteModel::where('id', '>', $copropriete->id)
            ->min('id');

        // Retrieve Pieces
        $piece_jointes = PieceJointeModel::fetch('copropriete', $copropriete->id, 'copropriete_images');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un copropriete - id: {$id}",'copropriete') ;

        return response()->json([   'success' => true,
                                    'copropriete'=> $copropriete,
                                    'piece_jointes' => $piece_jointes,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert Copropriete
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(CoproprieteFormRequest $request){

        /*******************************************
         * Insert new copropriete
         *******************************************/
        $copropriete = new CoproprieteModel();
        $copropriete->fk_syndic                 = $request->fk_syndic ;
        $copropriete->fk_copropriete_primaire   = $request->fk_copropriete_primaire ;
        $copropriete->exercice                  = $request->exercice ;
        $copropriete->status                    = 'active' ;

        $copropriete->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete' => ['insertedObject' => $copropriete]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout copropriete - id: {$copropriete->id}",'copropriete') ;

        return response()->json(['success' => true, 'updated_copropriete' => $copropriete]);
    }

    /**
     * Update Copropriete
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $copropriete
     *
     */
    public function update(CoproprieteFormRequest $request, $id){
        $copropriete = CoproprieteModel::findOrFail($id);
        $copropriete->fk_syndic                 = $request->fk_syndic ;
        $copropriete->fk_copropriete_primaire   = $request->fk_copropriete_primaire ;
        $copropriete->exercice                  = $request->exercice ;

        $copropriete->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete' => ['updatedObject' => $copropriete]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification copropriete - id: {$copropriete->id}",'copropriete') ;

        return response()->json(['success' => true, 
                                 'updated_copropriete' => $copropriete
                                ]);
    }

    /**
     * Delete Copropriete
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $copropriete_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to deleted
        \DB::table('copropriete')
            ->whereIn ('id', $copropriete_ids)
            ->update(['status'=> 'deleted']);


        /*******************************************
         * Delete Pieces Soft
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('copropriete', $copropriete_ids, 'copropriete_images');
        $piece_jointes = PieceJointeModel::deleteSoft($getPiecesIds);

        // Array to string
        $copropriete_ids = implode(', ',$copropriete_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['copropriete' => ['deleted_id' => $request->id]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression copropriete - id: { {$copropriete_ids} }",'copropriete') ;

        return response()->json(['success' => true, 
                                 'deleted_id' => $request->id, 
                                 'pieces'=> $piece_jointes
                               ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted coproprietes
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $copropriete
     *
     */
    public function fetchDeleted(Request $request){

        $coproprietes = DB::table('copropriete')
            ->select('copropriete.*')
            ->where([['copropriete.status', '=', 'deleted']])
            ->orderBy('copropriete.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les copropriete en corbeille",'copropriete') ;

        return response()->json(['success' => true, 
                                 'fetch_deleted_coproprietes' => $coproprietes
                                ]);
    }

    /**
     * Restore Copropriete
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $coproprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('copropriete')
            ->whereIn ('id', $coproprietes_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $coproprietes = ProprietaireModel::find($coproprietes_ids) ;
        // If coproprietes ID not found
        if($coproprietes->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $coproprietes_ids_str = implode(', ',$coproprietes_ids) ;

        /*******************************************
         * Restore Pieces
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('copropriete', $coproprietes_ids, 'copropriete_images');
        $piece_jointes = PieceJointeModel::restore($getPiecesIds);


        // Array to string
        $copropriete_ids_str = implode(', ',$coproprietes_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete' => ['restored_id' => $request->id,
            'restoredObjects' => $coproprietes]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration copropriete - id: [ {$coproprietes_ids} }",'copropriete') ;

        return response()->json(['success' => true, 'restored_id' => $coproprietes_ids, 'pieces'=> $piece_jointes]);
    }


    /**
     * Delete Permanently Copropriete(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $copropriete_ids = is_array($request->id) ? $request->id : [$request->id] ;

        $copropriete = CoproprieteModel::find($copropriete_ids[0]) ;
        // If Copropriete ID not found
        if(!$copropriete) return response()->json(['error' => 'not_found', 'message' => 'Copropriete ID not found - id: '.$copropriete_ids[0]]);

        /*******************************************
         * Delete Pieces Permanently
         ******************************************/
        $getPiecesIds  = PieceJointeModel::getPiecesIds('copropriete', $copropriete_ids, 'copropriete_images');
        $piece_jointes = PieceJointeModel::deletePermanently($getPiecesIds);

        CoproprieteModel::destroy($copropriete_ids);

        // Array to string
        $copropriete_ids_str = implode(', ', $request->id) ;

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement copropriete - id: { {$copropriete_ids_str} }",'copropriete') ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['copropriete' => ['deletedPermanently_id' => $request->id,]])));

        return response()->json(['success' => true, 
                                 'deletedPermanently_id' => $copropriete_ids, 
                                 'pieces'=> $piece_jointes
                               ]);
    }



    /**
     * Retrieve Coproprietes & Exercices
     *
     * @param $request
     *
     * @return array $coproprietes_primaire & $coproprietes_exercices
     *
     */
    public function getCoproprietesExercices (Request $request) {
        
        // Coproprietes primaire
        $coproprietes_primaire = DB::table('copropriete')
        ->select('copropriete_primaire.id as id_copropriete_primaire', 'copropriete_primaire.nom')
        ->join('copropriete_primaire', 
                'copropriete.fk_copropriete_primaire',
                '=',
                'copropriete_primaire.id')
        ->orderBy('copropriete_primaire.id', 'asc')
        ->distinct()
        ->get();

         //Coproprietés / Exercices
         $coproprietes_exercices = DB::table('copropriete')
         ->select('copropriete_primaire.id as id_copropriete_primaire', 
                  'copropriete_primaire.nom',
                  'copropriete.id as id_copropriete',
                  'copropriete.exercice')
         ->join('copropriete_primaire',
                 'copropriete.fk_copropriete_primaire',
                 '=',
                 'copropriete_primaire.id')
         ->distinct()
         ->orderBy('copropriete.exercice', 'asc')
         ->get();

        return response()->json(['success' => true, 
                                 'coproprietes_primaire' => $coproprietes_primaire, 
                                 'coproprietes_exercices' => $coproprietes_exercices
                                ]);
    }
}
