<?php

namespace App\Http\Controllers\ActivityLog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity ;


class ActivityLogController extends Controller
{

    /**
     * Liste activity_log
     *
     * @return array activity_log
     */
    function fetch()
    {
        $arr = [];
        $res = Activity::orderBy('id', 'desc')->get();

        foreach ($res as $item) {

            $id = $item->id ;
            $user_id = ($item->causer) ? $item->causer->id : '';
            $user_name = ($item->causer) ? $item->causer->name : '' ;
            $description = $item->description;
            $table = $item->getExtraProperty('table');
            $date = $item->getExtraProperty('date');
            $methode = $item->getExtraProperty('methode');
            $route = $item->getExtraProperty('route');
            $ip_address = $item->getExtraProperty('ip_address');

            $arr[] = [
                'id' => $id,
                'user_id' => $user_id,
                'user_name' => $user_name,
                'description' => $description,
                'table' => $table,
                'date' => $date,
                'methode' => $methode,
                'route' => $route,
                'ip_address' => $ip_address
            ];

        }

        return response()->json(['success' => true, 'activities'=> $arr]) ;
    }

}
