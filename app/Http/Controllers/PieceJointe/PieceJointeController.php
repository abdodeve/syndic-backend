<?php

namespace App\Http\Controllers\PieceJointe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PieceJointeModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PieceJointe\PieceJointeFormRequest ;
use Illuminate\Support\Facades\Storage;


class PieceJointeController extends Controller
{
    
 /*
 |-------------------------------------------------------------------------------------------------------------
 |  Primary
 |-------------------------------------------------------------------------------------------------------------
 */

    /**
     * Retrieve PieceJointes Of a Table passed by parameter
     *
     * @param $request $tableToJoin - (Table name to Join with PieceJoint) & $id - (ID row of tableJoined)
     *
     * @return array $piece_jointes
     *
     */
    public function fetch(Request $request){

        $piece_jointes = PieceJointeModel::fetch($request->tableToJoin, $request->tableToJoin_id);
        return response()->json([   'success' => true, 
                                    'piece_jointes' => $piece_jointes
                                ]);
    }



    /**
     * Insert PieceJointe
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(PieceJointeFormRequest $request){

        /*******************************************
         * Upload Files
         *******************************************/
        $piece  = $request->file('piece_jointe');
        $source = Storage::put('piece_jointe', $piece);
        $fk_tableToJoin = $request->input('fk_'.$request->tableToJoin);

        /*******************************************
         * Insert new piece_jointe
         *******************************************/
        $id_inserted = DB::table('piece_jointe')->insertGetId([
                             'fk_'.$request->tableToJoin => $fk_tableToJoin,
                             'name'                      => $piece->getClientOriginalName(),
                             'source'                    => $source,
                             'type'                      => $request->type,
                             'status'                    => 'active'
                        ]);
        $piece_jointe = PieceJointeModel::find($id_inserted);


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['piece_jointe' => ['insertedObject' => $piece_jointe]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout piece_jointe - id: {$piece_jointe->id}",'piece_jointe') ;

        return response()->json([   'success' => true, 
                                    'inserted_piece_jointe' => $piece_jointe
                                ]);
    }



    /**
     * Delete PieceJointe
     *
     * @param mixed $request (ids)
     *
     * @return int deleted_id
     *
     */
    public function deleteSoft(Request $request){

        // Check if array or int
        $piece_jointes_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Delete Soft
        $piece_jointes = PieceJointeModel::deleteSoft($piece_jointes_ids);

        // Array to string
        $piece_jointes_ids = implode(', ', $piece_jointes_ids);

        // Retrieve fk_copropriete
        $piece_jointe = PieceJointeModel::find($piece_jointes_ids[0]) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['piece_jointe' => ['deleted_id' => $request->id,
            'copropriete_id' => $piece_jointe->fk_copropriete,
            'deletedObjects' => $piece_jointes]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression piece_jointe - id: { {$piece_jointes_ids} }",'piece_jointe') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $request->id
                                ]);
    }



    /**
     * Delete Permanently PieceJointe
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function deletePermanently(Request $request){

        // Check if array or int
        $piece_jointes_ids = is_array($request->id) ? $request->id : [$request->id] ;

        // Delete files & records
        $isDeleted = PieceJointeModel::deletePermanently($piece_jointes_ids);

        if (!$isDeleted) return response()->json(['error' => 'not_found']);

        // Array ID to String
        $piece_jointes_ids = implode(', ', $piece_jointes_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['piece_jointe' => ['deleted_id' => $request->id]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement PieceJointe - id: { {$piece_jointes_ids} }",'piece_jointe') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $request->id
                                ]);
    }



    /**
     * Download PieceJointe
     *
     * @param mixed $request
     * @param int   $piece_id
     *
     * @return mixed - download files
     *
     */
    public function download(Request $request, $piece_id){
        // Retrieve PieceJointe with ID
        $piece_jointe = PieceJointeModel::find($piece_id);

        // If id not found
        if(!$piece_jointe) return response()->json(['error'=>'not_found']);

        $source     = $piece_jointe->source ;
        $file_name  = $piece_jointe->name ;

        // Download
        return Storage::download($source, $file_name) ;
    }

}
