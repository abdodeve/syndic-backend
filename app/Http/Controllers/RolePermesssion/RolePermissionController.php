<?php

namespace App\Http\Controllers\RolePermesssion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\RolePermissionModel ;
use App\Models\GroupePermissionsModel ;
use App\Models\RoleModel ;

class RolePermissionController extends Controller
{

    /**
     * Fetch permissionsRoles
     *
     * @param mixed $request (id_roleة id_cat_permission)
     *
     * @return array PermissionsRoles
     *
     */
    public function permissionsRole(Request $request){

        if(!$request->id_role || !$request->id_cat_permission)
            return response()->json(['error'=> 'id_role or id_cat_permission is Null']);

       $permissionsRole = RolePermissionModel::getPermissionsRoles($request) ;
       
        return response()->json([   'success' => true, 
                                    'permissionsRole' => $permissionsRole
                                ]);
    }

    /**
     * Fetch Roles & Groupe_permissions
     *
     * @param mixed $request (id_roleة id_cat_permission)
     *
     * @return array (roles, groupes)
     */
    public static function getRolesGroupes(){
        // Excluded Groupe
        $enabledModules = RolePermissionModel::enabledModules();
        $excluded_group = $enabledModules['grp_of_disabled_modules'] ;
        $excluded_group[] = 'super-admin-groupe' ;
        $roles = null ;
        $groupes = null ;

        // Retrieve $this User
        $user = \Auth::user() ;
        // Retrieve role name of this user
        $role_user = RoleModel::getRoleAuth();

        // If this User Is Super Admin
        if($role_user == 'super-admin'){

            $roles = RoleModel::orderBy('name')->get();
            $groupes = GroupePermissionsModel::orderBy('id')->get();

        }else { // If this User not Super Admin
            $roles = RoleModel::orderBy('name')
                    ->whereNotIn('name', ['super-admin', 'admin'])
                    ->get();

            $groupes = GroupePermissionsModel::orderBy('id')
                            ->whereNotIn('name', $excluded_group)
                            ->get();
        }

        return response()->json([   'success' => true, 
                                    'roles' => $roles,
                                    'groupes' => $groupes,
                                ]);
    }

    /**
     * Fetch Roles
     *
     * @return array roles
     */
    public function getRoles(){
        $ar = [ 'roles'=> RoleModel::orderBy('name')->get()];
        return response()->json([   'success' => true, 
                                    'getRoles' => $ar
                                ]);
    }

    /**
     * Assign & Revoke Permissions
     *
     * @param mixed $request (role_id, assign, remove)

     * @return string success msg
     */
    public function assign_permissions_to_role(Request $request) {
        // Assign permissions to Role
        $res = RolePermissionModel::syncRolePermission($request) ;

        return response()->json([   'success' => true,
                                    'assign_permissions_to_role' => $res,
                                    'message'=> 'syndic_role_has_permissions Synchronised'
                                ]) ;
    }

    /**
     * Add New Role
     *
     * @params mixed $request (name)

     * @return object role
     */
    public function addRole(Request $request) {
        //Avoid repeating this roles
        $roles_to_avoid = ['super-admin', 'admin'] ;
        if( in_array($request->name, $roles_to_avoid) )
            return response()->json(['error'=> 'admin_roles', 'message'=> 'You can\'t create super-admin, admin']);

        $res = RoleModel::create([ 'name' => $request->name]);

        return response()->json([   'success' => true,
                                    'message'=> 'Role added',
                                    'data'=> $res
                                ]);
    }

    /**
     * Add New Role
     *
     * @params mixed $request (id, name)

     * @return object role
     */
    public function updateRole(Request $request) {
        $r = RoleModel::find($request->id);
        $r->name = $request->name ;
        $r->save();
        return response()->json([   'success' => true,
                                    'message'=> 'Role updated',
                                    'data'=> $r
                                ]);
    }

    /**
     * Delete Role
     *
     * @params mixed $request (id)
     *
     * @return mixed role
     * @throws \Exception
     */
    public function deleteRole(Request $request){
        //Delete all affectation for this role
        $res = DB::table('role_has_permissions')
            ->where('role_id','=' , $request->id)
            ->delete();

        $r = RoleModel::find($request->id);
        $r->delete();

        return response()->json([   'success' => true, 
                                    'deletedRole'=> $r
                                ]);
    }

    /**
     * getPermissions - Func Controller
     * Get All Permissions of A User
     *
     * @params mixed $request (user_id)
     *
     * @return array $permissions
     *
     */
    public static function getPermissions(Request $request){
        $perm = RolePermissionModel::getPermissions($request);
        return response()->json([   'success' => true,
                                    'permissions'=> $perm
                                ]);
    }



}
