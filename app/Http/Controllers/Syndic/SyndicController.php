<?php

namespace App\Http\Controllers\Syndic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SyndicModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Syndic\SyndicFormRequest;

class SyndicController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste Syndics
     *
     * @param $request (id_copropriete)
     *
     * @return array $syndics
     *
     */
    public function fetch(Request $request){

        $syndics = DB::table('syndic')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'syndic.fk_copropriete'
            )
            ->select('syndic.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['syndic.status', '=', 'active']
            ])
            ->orderBy('syndic.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des syndics','syndic') ;

        return response()->json([   'success' => true, 
                                    'syndics' => $syndics
                               ]);
    }

    /**
     * Single Syndic
     *
     * @param int $id
     *
     * @return array $syndic, $previous, $next
     *
     */
    public function single(Request $request,$id) {
        $syndic = SyndicModel::find($id) ;
        // If id not found
        if(!$syndic) return response()->json(['error'=>'not_found']);

        // get previous syndic id
        $previous = SyndicModel::where('id', '<', $syndic->id)
            ->where('fk_copropriete', $syndic->fk_copropriete)
            ->max('id');

        // get next syndic id
        $next = SyndicModel::where('id', '>', $syndic->id)
            ->where('fk_copropriete', $syndic->fk_copropriete)
            ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un syndic - id: {$id}",'syndic') ;

        return response()->json([   'success' => true, 
                                    'syndic'=> $syndic,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert Syndic
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(SyndicFormRequest $request){

        /*******************************************
         * Insert new syndic
         *******************************************/

        $syndic = new SyndicModel();
        $syndic->fk_copropriete         = $request->fk_copropriete ;
        $syndic->code_syndic            = $request->code_syndic ;
        $syndic->nom_syndic             = $request->nom_syndic ;
        $syndic->email                  = $request->email ;
        $syndic->telephone_1            = $request->telephone_1 ;
        $syndic->telephone_2            = $request->telephone_2 ;
        $syndic->adresse                = $request->adresse ;
        $syndic->code_postale           = $request->code_postale ;
        $syndic->ville                  = $request->ville ;
        $syndic->pays                   = $request->pays ;
        $syndic->rc                     = $request->rc ;
        $syndic->patente                = $request->patente ;
        $syndic->carte_professionel     = $request->carte_professionel ;
        $syndic->capitale               = $request->capitale ;
        $syndic->description            = $request->description ;
        $syndic->logo                   = $request->logo ;
        $syndic->status                 = 'active' ;

        $syndic->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['syndic' => ['insertedObject' => $syndic]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout syndic - id: {$syndic->id}",'syndic') ;

        return response()->json([   'success' => true, 
                                    'inserted_syndic' => $syndic
                                ]);
    }

    /**
     * Update Syndic
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $syndic
     *
     */
    public function update(SyndicFormRequest $request, $id){
        $syndic = SyndicModel::find($id);

        // If proprietaire ID not found
        if(!$syndic) return response()->json(['error' => 'not_found', 'message' => 'Syndic ID not found - id: '.$id]);

        $syndic->fk_copropriete         = $request->fk_copropriete ;
        $syndic->code_syndic            = $request->code_syndic ;
        $syndic->nom_syndic             = $request->nom_syndic ;
        $syndic->email                  = $request->email ;
        $syndic->telephone_1            = $request->telephone_1 ;
        $syndic->telephone_2            = $request->telephone_2 ;
        $syndic->adresse                = $request->adresse ;
        $syndic->code_postale           = $request->code_postale ;
        $syndic->ville                  = $request->ville ;
        $syndic->pays                   = $request->pays ;
        $syndic->rc                     = $request->rc ;
        $syndic->patente                = $request->patente ;
        $syndic->carte_professionel     = $request->carte_professionel ;
        $syndic->capitale               = $request->capitale ;
        $syndic->description            = $request->description ;
        $syndic->logo                   = $request->logo ;
        $syndic->save();

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['syndic' => ['updatedObject' => $syndic]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification syndic - id: {$syndic->id}",'syndic') ;

        return response()->json([   'success' => true, 
                                    'updated_syndic' => $syndic
                                ]);
    }

    /**
     * Delete Syndic
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){
        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $syndics_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to delete
        \DB::table('syndic')
            ->whereIn ('id', $syndics_ids)
            ->update(['status'=> 'deleted']);

        // Retrieve fk_copropriete
        $syndic = SyndicModel::find($syndics_ids[0]) ;

        // If Syndic ID not found
        if(!$syndic) return response()->json(['error' => 'not_found', 'message' => 'Syndic ID not found - id: '.$syndics_ids[0]]);

        // Array to string
        $syndics_ids_str = implode(', ',$syndics_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['syndic' => ['deleted_id' => $syndics_ids,
            'copropriete_id' => $syndic->fk_copropriete,]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression syndic - id: { {$syndics_ids_str} }",'syndic') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $syndics_ids
                                ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted Syndics
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $syndic
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $syndics = DB::table('syndic')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'syndic.fk_copropriete'
            )
            ->select('syndic.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['syndic.status', '=', 'deleted']
            ])
            ->orderBy('syndic.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les syndics en corbeille",'syndic') ;

        return response()->json([   'success' => true,  
                                    'fetch_deleted_syndics' => $syndics
                                ]);
    }

    /**
     * Restore Syndic
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $syndics_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('syndic')
            ->whereIn ('id', $syndics_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $syndics = SyndicModel::find($syndics_ids) ;
        // If syndics ID not found
        if($syndics->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $syndics_ids_str = implode(', ',$syndics_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['syndic' => ['restored_id' => $syndics_ids,
            'copropriete_id' => $syndics[0]->fk_copropriete,
            'restoredObjects' => $syndics]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration syndic - id: [ {$syndics_ids_str} }",'syndic') ;

        return response()->json([   'success' => true, 
                                    'restored_id' => $syndics_ids
                                ]);
    }


    /**
     * Delete Permanently Syndic(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $syndics_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        $syndic        = SyndicModel::find($syndics_ids[0]) ;
        // If Syndic ID not found
        if(!$syndic) return response()->json(['error' => 'not_found', 'message' => 'Syndic ID not found - id: '.$syndics_ids[0]]);

        SyndicModel::destroy($syndics_ids);

        // Array to string
        $syndics_ids_str = implode(', ', $syndics_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['syndic' => ['deletedPermanently_id' => $syndics_ids,
            'copropriete_id' => $syndic->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement syndic - id: { {$syndics_ids_str} }",'syndic') ;

        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $syndics_ids
                                ]);
    }

}
