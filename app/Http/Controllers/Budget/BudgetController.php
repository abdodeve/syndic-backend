<?php

namespace App\Http\Controllers\Budget;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BudgetModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Budget\BudgetFormRequest;

class BudgetController extends Controller
{
  /*
  |-------------------------------------------------------------------------------------------------------------
  |  Primary
  |-------------------------------------------------------------------------------------------------------------
  */

    /**
     * Liste Budgets
     *
     * @param $request (id_copropriete)
     *
     * @return array $budgets
     *
     */
    public function fetch(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $budgets = DB::table('budget')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'budget.fk_copropriete'
            )
            ->select('budget.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['budget.status', '=', 'active']
            ])
            ->orderBy('budget.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des budgets','budget') ;

        return response()->json(['success' => true, 'budgets'=> $budgets]);
    }

    /**
     * Single Budget
     *
     * @param int $id
     *
     * @return array $budget, $previous, $next
     *
     */
    public function single(Request $request,$id) {

        $budget = BudgetModel::find($id) ;
        // If id not found
        if(!$budget) return response()->json(['error'=>'not_found']);

        // get previous budget id
        $previous = BudgetModel::where('id', '<', $budget->id)
            ->where('fk_copropriete', $budget->fk_copropriete)
            ->max('id');

        // get next budget id
        $next = BudgetModel::where('id', '>', $budget->id)
            ->where('fk_copropriete', $budget->fk_copropriete)
            ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un budget - id: {$id}",'budget') ;

        return response()->json([   'success' => true, 
                                    'budget'=> $budget,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert Budget
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(BudgetFormRequest $request){

        /*******************************************
         * Insert new budget
         *******************************************/
        $budget = new BudgetModel();
        $budget->fk_copropriete 				=	$request->fk_copropriete ;
        $budget->libelle						=	$request->libelle ;
        $budget->montant_actuel					=	$request->montant_actuel ;
        $budget->montant_anterieure 			=	$request->montant_anterieure ;
        $budget->montant_reste_anterieure		=	$request->montant_reste_anterieure ;
        $budget->status                         = 'active' ;

        $budget->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['budget' => ['insertedObject' => $budget]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout budget - id: {$budget->id}",'budget') ;

        return response()->json(['success' => true, 'inserted_budget' => $budget]);
    }

    /**
     * Update Budget
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $budget
     *
     */
    public function update(BudgetFormRequest $request, $id){

        $budget = BudgetModel::find($id);

        // If budget ID not found
        if(!$budget) return response()->json(['error' => 'not_found', 'message' => 'Budget ID not found - id: '.$id]);

        $budget->fk_copropriete 				=	$request->fk_copropriete ;
        $budget->libelle						=	$request->libelle ;
        $budget->montant_actuel					=	$request->montant_actuel ;
        $budget->montant_anterieure 			=	$request->montant_anterieure ;
        $budget->montant_reste_anterieure		=	$request->montant_reste_anterieure ;

        $budget->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['budget' => ['updatedObject' => $budget]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification budget - id: {$budget->id}",'budget') ;

        return response()->json(['success' => true, 'updated_budget' => $budget]);
    }

    /**
     * Delete Budget
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $budgets_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to delete
        \DB::table('budget')
            ->whereIn ('id', $budgets_ids)
            ->update(['status'=> 'deleted']);

        // Retrieve fk_copropriete
        $budget = BudgetModel::find($budgets_ids[0]) ;

        // If Budget ID not found
        if(!$budget) return response()->json(['error' => 'not_found', 'message' => 'Budget ID not found - id: '.$budgets_ids[0]]);

        // Array to string
        $budgets_ids_str = implode(', ',$budgets_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['budget' => ['deleted_id' => $budgets_ids,
            'copropriete_id' => $budget->fk_copropriete,]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression budget - id: { {$budgets_ids_str} }",'budget') ;

        return response()->json(['success' => true, 'deleted_id' => $budgets_ids]);
    }

    

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted Budgets
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $budget
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $budget = DB::table('budget')
            ->join('copropriete',
                'copropriete.id',
                '=',
                'budget.fk_copropriete'
            )
            ->select('budget.*')
            ->where([['copropriete.id', '=', $request->id_copropriete],
                ['budget.status', '=', 'deleted']
            ])
            ->orderBy('budget.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les budgets en corbeille",'budget') ;

        return response()->json(['success' => true, 'fetch_deleted_budgets' => $budget]);
    }

    /**
     * Restore Budget
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $budgets_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('budget')
            ->whereIn ('id', $budgets_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $budgets = BudgetModel::find($budgets_ids) ;
        // If budget ID not found
        if($proprietaires->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $budgets_ids_str = implode(', ',$budgets_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['budget' => ['restored_id' => $budgets_ids,
                                                                'copropriete_id' => $budgets[0]->fk_copropriete,
                                                                'restoredObjects' => $budgets]
                                                            ]
                                                )));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration budget - id: [ {$budgets_ids_str} }",'budget') ;

        return response()->json(['success' => true, 'restored_id' => $budgets_ids]);
    }


    /**
     * Delete Permanently Budget(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Int Or Array
        $budgets_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        $budget        = BudgetModel::find($budgets_ids[0]) ;
        // If Budget ID not found
        if(!$budget) return response()->json(['error' => 'not_found', 'message' => 'Budget ID not found - id: '.$budgets_ids[0]]);

        BudgetModel::destroy($budgets_ids);

        // Array to string
        $budgets_ids_str = implode(', ', $budgets_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['budget' => ['deletedPermanently_id' => $budgets_ids,
            'copropriete_id' => $budget->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement budget - id: { {$budgets_ids_str} }",'budget') ;

        return response()->json(['success' => true, 'deletedPermanently_id' => $budgets_ids]);
    }
}
