<?php

namespace App\Http\Controllers\TypeCopropriete;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TypeCoproprieteModel;
use App\Events\EventGlobale ;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\TypeCopropriete\TypeCoproprieteFormRequest;

class TypeCoproprieteController extends Controller
{

    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Primary
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Liste TypeCoproprietes
     *
     * @param $request (id_copropriete)
     *
     * @return array $typeCoproprietes
     *
     */
    public function fetch(Request $request){

        $typeCoproprietes = DB::table('type_copropriete')
            ->select('type_copropriete.*')
            ->where([['type_copropriete.status', '=', 'active']])
            ->orderBy('type_copropriete.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity('Affichage liste des typeCopropriete','type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'typeCopropriete'=> $typeCoproprietes
                                ]);
    }

    /**
     * Single TypeCopropriete
     *
     * @param int $id
     *
     * @return array $typeCopropriete, $previous, $next
     *
     */
    public function single(Request $request,$id) {
        $typeCopropriete = TypeCoproprieteModel::find($id) ;
        // If id not found
        if(!$typeCopropriete) return response()->json(['error'=>'not_found']);

        // get previous typeCopropriete id
        $previous = TypeCoproprieteModel::where('id', '<', $typeCopropriete->id)
                                        ->max('id');

        // get next typeCopropriete id
        $next = TypeCoproprieteModel::where('id', '>', $typeCopropriete->id)
            ->min('id');

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage d\'un typeCopropriete - id: {$id}",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'typeCopropriete'=> $typeCopropriete,
                                    'previous'=> $previous,
                                    'next'=> $next
                                ]);
    }

    /**
     * Insert TypeCopropriete
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     *
     * @return array $insertedObject
     *
     */
    public function insert(TypeCoproprieteFormRequest $request){

        /*******************************************
         * Insert new typeCopropriete
         *******************************************/
        $typeCopropriete = new TypeCoproprieteModel();
        $typeCopropriete->nom              = $request->nom ;
        $typeCopropriete->libelle          = $request->libelle ;
        $typeCopropriete->status           = 'active' ;

        $typeCopropriete->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['typeCopropriete' => ['insertedObject' => $typeCopropriete]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Ajout typeCopropriete - id: {$typeCopropriete->id}",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'inserted_typeCopropriete' => $typeCopropriete
                                ]);
    }

    /**
     * Update TypeCopropriete
     *
     * @param mixed $request (nom, prenom, fk_copropriete, status)
     * @param int $id
     *
     * @return object $typeCopropriete
     *
     */
    public function update(TypeCoproprieteFormRequest $request, $id){

        $typeCopropriete = TypeCoproprieteModel::find($id);
        // If typeCopropriete ID not found
        if(!$typeCopropriete) return response()->json(['error' => 'not_found', 'message' => 'Proprietaire ID not found - id: '.$id]);


        $typeCopropriete->nom              = $request->nom ;
        $typeCopropriete->libelle          = $request->libelle ;

        $typeCopropriete->save();


        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['typeCopropriete' => ['updatedObject' => $typeCopropriete]])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Modification typeCopropriete - id: {$typeCopropriete->id}",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'updated_typeCopropriete' => $typeCopropriete
                                ]);
    }

    /**
     * Delete TypeCopropriete
     *
     * @param mixed $request (id)
     *
     * @return int deleted_id
     *
     */
    public function delete(Request $request){
        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $type_coproprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to delete
        \DB::table('type_copropriete')
            ->whereIn ('id', $type_coproprietes_ids)
            ->update(['status'=> 'deleted']);

        // Retrieve fk_copropriete
        $type_copropriete = TypeCoproprieteModel::find($type_coproprietes_ids[0]) ;

        // If TypeCopropriete ID not found
        if(!$type_copropriete) return response()->json(['error' => 'not_found', 'message' => 'TypeCopropriete ID not found - id: '.$type_coproprietes_ids[0]]);

        // Array to string
        $type_coproprietes_ids_str = implode(', ',$type_coproprietes_ids) ;

        /*******************************************
         * Send Event
         ******************************************/
        event(new EventGlobale(response()->json(['type_copropriete' => ['deleted_id' => $type_coproprietes_ids,
            'copropriete_id' => $type_copropriete->fk_copropriete,]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression type_copropriete - id: { {$type_coproprietes_ids_str} }",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'deleted_id' => $type_coproprietes_ids
                                ]);
    }


    /*
    |-------------------------------------------------------------------------------------------------------------
    |  Restauration
    |-------------------------------------------------------------------------------------------------------------
    */

    /**
     * Fetch deleted TypeCoproprietes
     *
     * @param mixed $request (id_copropriete)
     *
     * @return object $typeCopropriete
     *
     */
    public function fetchDeleted(Request $request){

        // Validate Inputs
        $request->validate([
            'id_copropriete'   => 'required',
        ]);

        $typeCoproprietes = DB::table('type_copropriete')
            ->select('type_copropriete.*')
            ->where(['type_copropriete.status', '=', 'deleted'])
            ->orderBy('type_copropriete.id', 'desc')
            ->get();

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Affichage les type_coproprietes en corbeille",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'fetch_deleted_typeCopropriete' => $typeCoproprietes
                                ]);
    }

    /**
     * Restore TypeCopropriete
     *
     * @param mixed $request (id)
     *
     * @return mixed $restored_id
     *
     */
    public function restore(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $type_coproprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        // Change status to active
        \DB::table('type_copropriete')
            ->whereIn ('id', $type_coproprietes_ids)
            ->update(['status'=> 'active']);

        // Retrieve restoredObjects
        $type_coproprietes = TypeCoproprieteModel::find($type_coproprietes_ids) ;
        // If type_coproprietes ID not found
        if($type_coproprietes->isEmpty()) return response()->json(['error' => 'not_found']);

        // Array to string
        $type_coproprietes_ids_str = implode(', ',$type_coproprietes_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['type_copropriete' => ['restored_id' => $type_coproprietes_ids,
            'copropriete_id' => $type_coproprietes[0]->fk_copropriete,
            'restoredObjects' => $type_coproprietes]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Restauration type_copropriete - id: [ {$type_coproprietes_ids_str} }",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'restored_id' => $type_coproprietes_ids
                                ]);
    }


    /**
     * Delete Permanently TypeCopropriete(s)
     *
     * @param mixed $request (id)
     *
     * @return array $deletedPermanently_id
     *
     */
    public function deletePermanently(Request $request){

        // Validate Inputs
        $request->validate([
            'id'   => 'required',
        ]);

        // Check if array or int
        $type_coproprietes_ids   = is_array($request->id) ? $request->id : [$request->id] ;

        $type_copropriete        = TypeCoproprieteModel::find($type_coproprietes_ids[0]) ;
        // If TypeCopropriete ID not found
        if(!$type_copropriete) return response()->json(['error' => 'not_found', 'message' => 'TypeCopropriete ID not found - id: '.$type_coproprietes_ids[0]]);

        TypeCoproprieteModel::destroy($type_coproprietes_ids);

        // Array to string
        $type_coproprietes_ids_str = implode(', ', $type_coproprietes_ids) ;

        /*******************************************
         * Send Event
         *******************************************/
        event(new EventGlobale(response()->json(['type_copropriete' => ['deletedPermanently_id' => $type_coproprietes_ids,
            'copropriete_id' => $type_copropriete->fk_copropriete]
        ])));

        /*******************************************
         * Save Activity_Log
         *******************************************/
        the_activity("Suppression définitivement type_copropriete - id: { {$type_coproprietes_ids_str} }",'type_copropriete') ;

        return response()->json([   'success' => true, 
                                    'deletedPermanently_id' => $type_coproprietes_ids
                                ]);
    }

}
